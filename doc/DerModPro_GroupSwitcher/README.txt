/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

==== ABOUT ====
This extension allows you to automatically assign customers to customer groups,
according to configurable rules.

The following rule types are available:

- Purchase of product
When a customer purchases a specified product he is assigned to the
configured customer group. The order state when the assignment should happen can
be configured (e.g. New, Processing, Complete, ...).
All product types are supported.

- Number of orders
As soon as the number of orders from a customer reach the specified amount he is
assigned to the group you configured. You can limit the orders to count to a
specific state (e.g. Processing, Complete, ...).
You can also limit the timeframe in which orders are counted (e.g. only count
orders within the last 60 days). You can also configure a rule switch when the
number of orders within a timeframe drop below a certain amount.

- Single order total
If the subtotal of an order is higher (or lower, if you want) then the configured
value, the customer is assigned to the specified group. Possible uses would be
to give customers a discount as long as their last order was higher then some
amount. You can also configure a state the order must match (e.g. New,
Processing, Complete, ...).

- Turnover of customer
This rule type can evaluate if the total subtotal turnover of all orders from a
customer is over (or under) a specific value. You can limit the timeframe from
which orders are counted (e.g. only change the customers group if his turnover
within the last 45 days is higher then 500,-). The order state can also be
specified (e.g. orders have to be complete for the rule to evaluate them).

- Customer attribute
When a customer attribute matches some value he is assigned to the configured
group. You can configure the matching to be done as a simple value comparison, a
regular expression or, if you are a magento developer, you can specify a model
callback to check the customer matches the required value. For syntax details
check the description below in the USAGE section of this document.

- Customer VAT Identification Number
This rule is mainly useful for stores within the European Union. If a customer
specifies a valid vat id number, he can be assigned to a customer group with the
correct tax class for his country.
If the store owner possesses a german vat id number, the customers vat id can be
checked using an online XML-RPC service from http://evatr.bff-online.de/ when
the customer is from an EU country outside of Germany. See the USAGE section
below how to set up this feature.

All rules support the configuration of a group the customer must be in for the
rule to match.
Also, you can configure a group switch for some time in the future when a rule
matches. A possible use for these scheduled group switches could be, when a
customer purchases some product, he is placed in a customer group with
better pricing for 30 days. Once the configured timeframe passes, the customer is
automatically assigned back to previous group again.

Another optional feature all rules provide is sending customer notification
emails when a group change occurs. The transactional email template to use can
be configured for every rule.


==== INSTALLATION ====
1.) Unzip the archive in the root directory of your Magento installation.
2.) Refresh the configuration and HTML-Block Cache
3.) Log out of the admin-interface
4.) Log back in to the admin interface

You can check the installation was successful if you have a new menu entry
"Group Switcher" under "Customers".


==== CONFIGURATION ====
The extension is very easy to configure, there are only very few options.
The options can be found at
System > Configuration > Der Modulprogrammierer > GroupSwitcher

You have the following configuration options:

- Customer Notification Email Sender
Select the email identity to use when sending customer notification emails.
Of course this will only take effect if you configure your rules to send
notifications when a group switch is applied.

- Enable GroupSwitcher Logging
If enabled all groupswitches and errors will be logged to the file
var/log/groupswitcher.log.

- Store VAT Identification Number
If the store owner possesses a EU vat id number, it can be entered here to
activate the more advanced vat id validation types (see below).
Moreover, online validation of vat id's using the XML-RPC service at
http://evatr.bff-online.de/ will be used if the stores vat id is german, and a
customers specifies a non-german vat id. The service only supports the
validation of vat id's from outside of Germany in conjunction with a valid German
vat id. If this field is left empty, vat id numbers are still checked for syntactic
correctness using the rules from
http://ec.europa.eu/taxation_customs/vies/faqvies.do#item11


==== USAGE ====
In order for the extension to do anything, you first need to configure some
group switch rules. To do so, log into the admin panel and navigate to
Customers > Group Switcher > Group Switcher Rules.

=== Group Switcher Rules ===

Here you see a list with all configured rules. The first time you visit the page,
the list will, of course, be empty.
In order to create a rule click the "Add New Rule" button. The first thing to
configure is the rule type. To see a description of the available rule types
please read the ABOUT section above.
After you select a rule type, you are prompted with a form in the familiar
Magento look and feel. Notice the three tabs on the left side.
The form fields on the first and the third tab are the same for all rule types.
The middle tab contains the configuration that is different for each rule type.
Most fields should be self-explanatory, but maybe not all, so here is a
description of all available fields, grouped by the tab they are found on.


== Fields on the "General" tab ==

- Name
The rule name exists so you have an easy way to identifying rules you create.
It is best to enter a descriptive name.

- Switch from Group
If you select a customer group in this drop-down, the rule will only apply to
customers who are in this specific group.

- Switch to Group
If the rule matches, the customer will be assigned to this customer group.

- Is Active
You can deactivate a rule to easily disable it without having to delete it.

- Priority
If you configure several possible rules they will be processed in order of the
priority. Rules with a higher priority value are processed first.

- Stop Processing
If set to yes, no further rules will be processed if this rule matches.

- Send customer email when setting new group
Set this to "Yes" if you want to notify the customer about the group change.

- Email Template
This field is only visible if you set "Send customer email when setting new group"
to "Yes". Here you can configure a transactional email template to use for the
notification. The GroupSwitcher extension comes with a default template that
must be customized for production use.

- Comment
This field is for your notes. They are only visible when you edit a rule. You
can leave it empty if you don't need it.


== Fields on the "Scheduling" tab ==

- Schedule a group switch
Set this to "Yes" if you want to schedule a future group switch when this rule
matches. All other fields on this tab are only visible if "Yes" is selected.

- Schedule Switch in N days
Enter the number of days from the time the rule matches that the scheduled switch
should happen.

- Schedule Switch: from Group
The scheduled switch will only be applied if the customer is assigned to this
customer group at the time the switch is scheduled to happen. This is to
safeguard against accidental group switches.

- Schedule Switch: to Group
Assign the customer to this group when processing the scheduled switch.

- Replace other scheduled switches for the customer
If set to yes, all other scheduled switches for the customer will be replaced
by this one when the rule matches.

- Send customer email when setting new group
Set this to "Yes" if you want to notify the customer via email when the scheduled
switch is processed.

- Email Template
This field is only visible if you set "Send customer email when setting new group"
to "Yes". You can configure a transactional email template to use for the
notification here. The GroupSwitcher extension comes with a default template that
must be customized for production use.


== Fields on the "Type Specific" tab ==

These fields depend on the rule type selected when creating the rule.

= Type specific options for "Purchase of product" rules =

- Product SKU
Enter a list of SKU that should trigger the group switch. Several SKU's can be
separated with a comma followed by a space.
A single comma might be used as part of a SKU, so is it is not enough.

- Order State
The rule will only matches when the order is in this state.

- Store IDs
The rule will only match for orders placed from the selected the Magento store
views. Be sure to select all views if you want to make the rule global.

= Type specific options for "Number of orders" rules =

- Number of Orders are
Select the comparison operator to use. Available are: Equal, Greater then,
Less then, Greater then or equal, Less then or equal.
The number of orders of the customer is compared to the value specified in the
field "Order Count" using this operator .

- Order Count
The customers number of orders is compared to this value, using the comparison
operator specified above.

- Within the last N Days
Only count orders placed within the number of days specified here. Leave this
field empty or set it to 0 (zero) if you don't want a time limit on the counted
orders.

- Order State
Only orders in this state are counted.

= Type specific options for "Single order total" rules =

- Order Subtotal is
Select the comparison operator to use. Available are: Equal, Greater then,
Less then, Greater then or equal, Less then or equal.
The order subtotal is compared to the value specified in the field "Amount"
using this operator.

- Amount
The order subtotal is compared to this value, using the comparison operator
specified above.

- Order State
The rule only matches once the order is in this state.

- Store IDs
The rule will only match for orders placed from the selected the Magento store
views. Be sure to select all views if you want to make the rule global.

= Type specific options for "Turnover of customer" rules =

- Total Turnover for Customer is
Select the comparison operator to use. Available are: Equal, Greater then,
Less then, Greater then or equal, Less then or equal.
The subtotal turnover of the customer is compared to the value specified in the
field "Amount" using this operator.

- Within the last N Days
Only use orders placed within the number of days specified here. Leave this
field empty or set it to 0 (zero) if you don't want a time limit.

- Order State
The rule only uses orders is in the this state to calculate the customers
turnover.

- Store IDs
The rule will only use orders placed from the selected the Magento store views
to calculate the customers turnover. Be sure to select all views if you want to
make the rule global.

= Type specific options for "Customer attribute" rules =

- Customer Attribute
Choose the customer attribute you want the rule to check.

- Matches
Enter the value the customer attribute will be matched against. What to enter
depends on the selection of the next field "Match Expression Type"

- Match Expression Type
Select the match type. Available are: Plain value, Regular Expression, Model
Callback.

If you select "Plain value", the rule will compare the customers attribute value
to the value entered in the "Matches" field and match if the values are identical.

If you select "Regular Expression", the customer attribute will check the
value entered under "Matches" as a Regex. Regular expressions are a complex
subject not covered by this document. Further instructions can be found at
http://www.php.net/manual/en/reference.pcre.pattern.syntax.php
A regular expression example could be: /[a-Z0-5]{2,6}/
This would match 2 to 6 characters and/or digits from 0 to 5.

If you have some experience developing magento extensions, you may create a
model and specify a callback function if you select "Model Callback" as the match
type. The syntax for value entered in the "Matches" field is
module/model::method
Developer information: Three arguments are passed to the callback method, the first
is the customer model and the second is the selected attribute code, and the third
is the rule model. This option gives you the most flexibility, but you clearly
need to know what you are doing. Creating Magento models is not covered by this
document, but there are many tutorials available online, such as
http://www.magentocommerce.com/knowledge-base/entry/magento-for-dev-part-1-introduction-to-magento

= Type specific options for "Customer VAT Identification Number" rules =

- VAT ID Number Attribute
Select the customer attribute that is used for the vat id number. This defaults
to "taxvat" (TAX/VAT Number).

- Apply rule if
Select how to evaluate if the rule matches the customers vat id.

Select "VAT ID is valid and from foreign EU country" to trigger a match if the
customer has entered a valid vat id from a country different from the vat id
configured in the system configuration at " System > Configuration >
GroupSwitcher > Store VAT Identification Number. This match type is only
available when a store VAT id is configured.
The match Type "VAT ID is not valid and from foreign EU country" matches if the
customers vat id is either invalid or if it is valid and from the same country
as the store vat id.
To match a valid inland vat ID, choose the match type "VAT ID is valid and from
same EU country".
To match an invalid or foreign vat id select "VAT ID is not valid and from same
EU country".
The previous four match types are only available when the store vat id has been
configured.
Select "VAT ID is valid" if you want the rule to match customers with a valid
vat id.
Select "VAT ID is not valid" if you want the rule to match customers
with invalid vat id.



=== Scheduled Group Switches ===

You can manage the scheduled group switches by logging into the admin panel and
navigating to "Customers > Group Switcher > Scheduled Group Switches".
The first screen is a list of all scheduled group switches.
Most scheduled group switches will be created by GroupSwitcher rules. But if you
need to, you can also create entries manually.

In order for the the scheduled switches to be processed, Magento Cron jobs need
to be configured. This is not covered in this document, but you can find
instructions at
http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/how_to_setup_a_cron_job

When creating or editing existing scheduled group switches, you will see the
following fields.

- Customer
You can specify a customer by entering either the customer-ID or the email address.

- Website ID
Select the website ID the customer is assigned to. This is required for technical
reasons to load the customer correctly.

- Switch at
The date the scheduled switch will be processed can specified here. A date picker
is available if you click on the square icon beside the field.

- Switch from Group
The scheduled switch will only be processed if the customer is in the selected
customer group at the specified date. If you want the customer assigned to the
new group regardless of his current group, select "Any".

- Switch to Group
The customer will be assigned to this group when the scheduled group switch is
processed.

- Send customer email when setting new group
Set this to "Yes" if you want the customer to be notified via email.

- Email Template
This field is only visible if you set "Send customer email when setting new group"
to "Yes". Here you can configure a transactional email template to use for the
notification. The GroupSwitcher extension comes with a default template that
must be customized for production use.

- Comment
If you want to, you can enter comments here. Scheduled switches created by
GroupSwitcher rules will have a comment specifying the creating rules name and
ID. Once a scheduled switch is processed, this information is also added to the
comment field.


==== SUPPORT ====
Deutsch: http://www.der-modulprogrammierer.de/hilfeseiten/hilfe/groupswitcher-de.html
English: http://www.extensionprogrammer.com/hilfeseiten/hilfe/groupswitcher-en.html


==== BUGS ====
If you have ideas for improvements or find bugs, please send them to info@der-modulprogrammierer.de,
with DerModPro_GroupSwitcher as part of the email subject.
