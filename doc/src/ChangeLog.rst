.. footer::
   .. class:: footertable
   
   +-------------------------+-------------------------+
   | Stand: 14.05.2012       | .. class:: rightalign   |
   |                         |                         |
   |                         | ###Page###/###Total###  |
   +-------------------------+-------------------------+

.. sectnum::

=============
GroupSwitcher
=============

ChangeLog
=========

.. list-table::
   :header-rows: 1
   :widths: 1 1 6

  *  - Revision
     - Datum
     - Beschreibung

  * - 13.01.09
    - 09.01.2013
    - Bugfixes:

       * fixed Number of Orders filter 
       * fix "From group" is not used as a filter

  * - 13.01.09
    - 09.01.2013
    - Changes:

       * Show information block in configuration area
       * changed versions number to new convention YY.MM.DD

  *  - 0.1.8
     - 08.10.2012
     - Changes:

       * add LICENSE.txt
       * changed license header

  *  - 0.1.7
     - 14.05.2012
     - Bugfixes:

       * Fixed NULL vs. '' check of order states for turnover rules
       * Fixed NULL vs. '' check of order states for turnover rules

