GroupSwitcher Features
======================

Feature: GroupSwitcher
----------------------

  :*Mit dem Ziel*:        Zugehörigkeit zu Kundengruppen einfacher zu verwalten
  :*als*:                 Händler
  :*vorausgesetzt*:       bestimmte Ereignisse wurden definiert
  :*möchte ich, dass*:    Kunden automatisch einer Kundengruppe zugewiesen werden

Regeltypen
----------

  1. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Purchase of product" angelegt
    :*Wenn ich*:      das spezifizierte Produkt kaufe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe nach dem Bestellabschluss geändert

  2. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Purchase of product" angelegt
    :*Wenn ich*:      eine Bestellung im Backend anlege, welche das
                      spezifizierte Produkt enthält
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  3. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Number of orders" angelegt
    :*Wenn ich*:      durch den Abschluss einer neuen Bestellung die
                      spezifizierte Anzahl an Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  4. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Number of orders" angelegt
    :*Wenn ich*:      mich im Shop einlogge und die spezifizierte Anzahl an
                      Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  5. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Number of orders" angelegt
    :*Wenn ich*:      eine Bestellung im Backend anlege und der Kunde damit die
                      spezifizierte Anzahl an Bestellungen erreicht hat
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  6. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Single order total" angelegt
    :*Wenn ich*:      eine Bestellung abschließe, deren Zwischensumme der
                      spezifizierten Zwischensumme entspricht
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  7. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Single order total" angelegt
    :*Wenn ich*:      eine Bestellung im Backend anlege, deren Zwischensumme der
                      spezifizierten Zwischensumme entspricht
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  8. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Turnover of customer" angelegt
    :*Wenn ich*:      nach einem Bestellabschluss alle meine Bestellungen den
                      spezifizierten Gesamtumsatz erreicht haben
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  9. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Turnover of customer" angelegt
    :*Wenn ich*:      eine Bestellung im Backend anlege und alle Bestellungen
                      des Kunden den spezifizierten Gesamtumsatz erreicht haben
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  10. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Turnover of customer" angelegt
    :*Wenn ich*:      mich im Shop einlogge und alle meine Bestellungen den
                      spezifizierten Gesamtumsatz erreicht haben
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  11. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer attribute" angelegt
    :*Wenn ich*:      mich einlogge und das spezifizierte Attribut auf meinen
                      Account zutrifft
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  12. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer attribute" angelegt
    :*Wenn ich*:      mich registriere und das spezifizierte Attribut auf meinen
                      Account zutrifft
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  13. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer attribute" angelegt
    :*Wenn ich*:      meine Kundendaten ändere und das spezifizierte Attribut
                      auf meinen Account zutrifft
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  14. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer attribute" angelegt
    :*Wenn ich*:      einen Kunden im Backend anlege und das spezifizierte
                      Attribut auf den Kundenaccount zutrifft
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  15. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer attribute" angelegt
    :*Wenn ich*:      die Kundendaten eines Kundenaccounts im Backend ändere und
                      das spezifizierte Attribut auf den Kundenaccount zutrifft
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  16. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer VAT Identification
                      Number" angelegt
    :*Wenn ich*:      mich registriere und die Ust-Regel greift
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  17. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer VAT Identification
                      Number" angelegt
    :*Wenn ich*:      meine Kundendaten ändere und die Ust-Regel greift
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe geändert

  18. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer VAT Identification
                      Number" angelegt
    :*Wenn ich*:      einen Kunden im Backend anlege und die Ust-Regel greift
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  19. Szenario:

    :*Vorausgesetzt*: es wurde eine Regel vom Typ "Customer VAT Identification
                      Number" angelegt
    :*Wenn ich*:      die Kundendaten eines Kundenaccounts im Backend ändere und
                      die Ust-Regel greift
    :*als*:           Shopbetreiber
    :*dann*:          wird die Kundengruppe des Kunden geändert

  20. Szenario:

    :*Vorausgesetzt*: es wurden zwei Regeltypen (bspw. "Turnover of customer"
                      und "Number of orders") angelegt mit unterschiedlichen
                      Ziel-Kundengruppen und Prioritäten, wobei die Ausführung
                      nicht angehalten werden soll
    :*Wenn ich*:      den spezifizierten Gesamtumsatz nicht erreicht habe und
                      die spezifizierte Anzahl an Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe gemäß derjenigen Regel geändert,
                      für welche die Bedingungen erfüllt waren.

Allgemeine Einstellungen
------------------------

  1. Szenario:

    :*Vorausgesetzt*: es wurden zwei Regeltypen (bspw. "Turnover of customer"
                      und "Number of orders") angelegt mit unterschiedlichen
                      Ziel-Kundengruppen und Prioritäten, wobei die Ausführung
                      nicht angehalten werden soll
    :*Wenn ich*:      den spezifizierten Gesamtumsatz und die spezifizierte
                      Anzahl an Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe auf diejenige mit dem niedrigeren
                      Prioritätswert geändert, da die Regel mit höherem
                      Prioritätswert zuerst abgearbeitet wurde.

  2. Szenario:

    :*Vorausgesetzt*: es wurden zwei Regeltypen (bspw. "Turnover of customer"
                      und "Number of orders") angelegt mit unterschiedlichen
                      Ziel-Kundengruppen und Prioritäten, wobei die Ausführung
                      nicht angehalten werden soll und die Regel mit niedrigerem
                      Prioritätswert nicht aktiviert ist
    :*Wenn ich*:      den spezifizierten Gesamtumsatz und die spezifizierte
                      Anzahl an Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe auf diejenige mit dem höheren
                      Prioritätswert geändert, da die Regel mit niedrigerem
                      Prioritätswert deaktiviert wurde.

  3. Szenario:

    :*Vorausgesetzt*: es wurden zwei Regeltypen (bspw. "Turnover of customer"
                      und "Number of orders") angelegt mit unterschiedlichen
                      Ziel-Kundengruppen und Prioritäten, wobei nach Ausführung
                      der Regel mit höherem Prioritätswert angehalten werden soll
    :*Wenn ich*:      den spezifizierten Gesamtumsatz und die spezifizierte
                      Anzahl an Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe auf diejenige mit dem höheren
                      Prioritätswert geändert, da "Ausführung anhalten" die
                      Anwendung der Regel mit niedrigerem Prioritätswert verhindert.

  4. Szenario:

    :*Vorausgesetzt*: es wurden zwei Regeltypen (bspw. "Turnover of customer"
                      und "Number of orders") angelegt mit unterschiedlichen
                      Ziel-Kundengruppen und Prioritäten, wobei nach Ausführung
                      der Regel mit höherem Prioritätswert angehalten werden
                      soll und die Regel mit höherem Prioritätswert nicht
                      aktiviert ist
    :*Wenn ich*:      den spezifizierten Gesamtumsatz und die spezifizierte
                      Anzahl an Bestellungen erreicht habe
    :*als*:           Kunde
    :*dann*:          wird meine Kundengruppe auf diejenige mit dem niedrigeren
                      Prioritätswert geändert, da "Ausführung anhalten" einer
                      deaktivierten Regel keinen Effekt hat.
