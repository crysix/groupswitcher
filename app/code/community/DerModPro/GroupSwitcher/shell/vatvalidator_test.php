<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

require_once dirname(__FILE__) . '/../../../../../../shell/abstract.php';

class Vat_Validator_Test extends Mage_Shell_Abstract
{
	public function run()
	{
		if (Mage::getModel('GroupSwitcher/validator_vatId')->test())
		{
			printf("[SYNTAX]  All vat id numbers validated\n");
		}

		/*
		 * Test xml-rpc validation
		 */
		try
		{
			if (Mage::getModel('GroupSwitcher/validator_vatId_rpc')->test())
			{
				printf("[XML-RPC] All vat id numbers validated\n");
			}
		}
		catch (Exception $e)
		{
			printf("Error during xml-rpc validation tests:\n%s\n", $e->getMessage());
		}
	}
}


ini_set('display_errors', 1);

$tester = new Vat_Validator_Test();

Mage::setIsDeveloperMode(true);

$tester->run();