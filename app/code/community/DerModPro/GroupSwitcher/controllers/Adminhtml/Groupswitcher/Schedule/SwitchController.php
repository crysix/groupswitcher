<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Adminhtml_Groupswitcher_Schedule_SwitchController
	extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$helper = Mage::helper('GroupSwitcher');

		$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($helper->__('Schedule'));

		$this->loadLayout();

		$this->_setActiveMenu('customer/group/groupswitcher_schedule');

		$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
		$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
		$this->_addBreadcrumb($helper->__('Schedule'), $helper->__('Schedule'));

		$this->renderLayout();
	}

	public function newAction()
	{
		$this->_forward('edit');
	}

	public function editAction()
	{
		$helper = Mage::helper('GroupSwitcher');
		$model = Mage::getModel('GroupSwitcher/schedule_switch');
		$id = $this->getRequest()->getParam('id');
		$errorRedirectPath = '*/*';

		try
		{
			if ($id && ! $model->load($id)->getId())
			{
				Mage::throwException(Mage::helper('GroupSwitcher')->__('No record with ID "%s" found.', $id));
			}

			Mage::register('groupswitcher_switch', $model);

			/*
			 * Build the page title
			 */
			if ($model->getId())
			{
				$pageTitle = $helper->__('Edit Scheduled Switch #%d', $model->getId());
			}
			else
			{
				$pageTitle = $helper->__('New Scheduled Switch');
			}
			$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($pageTitle);

			$this->loadLayout();

			$this->_setActiveMenu('customer/group/groupswitcher_schedule');
			
			$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
			$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
			$this->_addBreadcrumb($pageTitle, $pageTitle);

			$this->renderLayout();
		}
		catch (Mage_Core_Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect($errorRedirectPath);
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect($errorRedirectPath);
		}
	}

	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost())
		{
			$this->_getSession()->setFormData($data);

			$model = Mage::getModel('GroupSwitcher/schedule_switch');

			$id = $this->getRequest()->getParam('id');

			try
			{
				if ($model->load($id)->getId())
				{
					$data['created_at'] = $model->getCreatedAt();
					$data['updated_at'] = $model->getUpdatedAt();
					$this->_getSession()->setFormData($data);
				}
				$model->addData($data);
				if ($id) $model->setId($id);

				$model->save();

				if (! $model->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcher')->__('Error saving scheduled switch'));
				}

				$this->_getSession()->addSuccess(Mage::helper('GroupSwitcher')->__('Scheduled switch was successfully saved'));
				$this->_getSession()->setFormData(false);

				if ($this->getRequest()->getParam('back'))
				{
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
				}
				else
				{
					$this->_redirect('*/*/');
				}
			}
			catch (Exception $e)
			{
				$this->_getSession()->addError($e->getMessage());
				if ($model && $model->getId())
				{
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
				}
				else
				{
					$this->_redirect('*/*/');
				}
			}

			return;
		}

		$this->_getSession()->addError(Mage::helper('GroupSwitcher')->__('No data found to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction()
	{
		$model = Mage::getModel('GroupSwitcher/schedule_switch');
		$id = $this->getRequest()->getParam('id');

		try
		{
			if ($id)
			{
				if (! $model->load($id)->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcher')->__('No record with ID "%s" found.', $id));
				}
				$model->delete();
				$this->_getSession()->addSuccess(Mage::helper('GroupSwitcher')->__('Scheduled Switch with ID %d was successfully deleted', $id));
				$this->_redirect('*/*');
			}
		}
		catch (Mage_Core_Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect('*/*');
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect('*/*');
		}
	}
}

