<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Observer
{
    /**
     * Trigger all order related group switches
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderStatusHistorySaveAfter(Varien_Event_Observer $observer)
    {
		$orderStatus = $observer->getEvent()->getStatusHistory();
		$order = $orderStatus->getOrder();
                if($order == null)
                {
                    /*
                     * May happen if notifications are sent
                     * @see GS-13
                     */
                    $data = $orderStatus->getData();
                    if($data['entity_name'] != 'order'){
                        /*
                         * Item is no order
                         */
                        return;
                    } else {
                        $order = Mage::getModel('sales/order')->load($data['parent_id']);
                    }
                }
                
		if (! $order->getCustomerId())
		{
			/*
			 * Guest order
			 */
			return;
		}

		$storeId = $order->getStoreId();

		if (! Mage::helper('GroupSwitcher')->getConfig('enable_ext', $storeId))
		{
			/*
			 * Module deactivated for the order store config scope
			 */
			return;
		}

		$this->_applyRules('order_update', $order);
    }

    /**
     * Trigger all customer attribute related group switches
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerSaveAfter(Varien_Event_Observer $observer)
    {
		$customer = $observer->getEvent()->getCustomer();

		$storeId = $customer->getStoreId() ? $customer->getStoreId() : Mage::app()->getStore()->getId();

		if (! Mage::helper('GroupSwitcher')->getConfig('enable_ext', $storeId))
		{
			/*
			 * Module deactivated for the customer store config scope
			 */
			return;
		}

		$this->_applyRules('customer_update', $customer);
    }

    /**
     * Trigger group switches when customer logs in
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerLogin(Varien_Event_Observer $observer)
    {
		$customer = $observer->getEvent()->getCustomer();

		if (! $customer->getId())
		{
			/*
			 * Guest? Can't see how that should be possible, but why not be a little paranoid
			 */
			return;
		}
        
		$storeId = Mage::app()->getStore()->getId();
        
		if (! Mage::helper('GroupSwitcher')->getConfig('enable_ext', $storeId))
		{
			/*
			 * Module deactivated for the customer store config scope
			 */
			return;
		}

		$this->_applyRules('login', $customer);
    }

    /**
     *
     * @param string $eventType
     * @param Varien_Object $object
	 * @return DerModPro_GroupSwitcher_Model_Observer
     */
	protected function _applyRules($eventType, Varien_Object $object = null)
	{
		if (isset($object) && $object->getSkipGroupswitcherEvents())
		{
			return;
		}
		
        if ($ruleTypes = $this->_getRuleTypesByEvent($eventType))
		{
			$ruleCollection = Mage::getResourceModel('GroupSwitcher/rule_collection')
				->addActiveFilter()
				->addRuleTypeFilter($ruleTypes)
				->addOrder('priority', Varien_Data_Collection_Db::SORT_ORDER_DESC)
			;

			$this->_addObjectFiltersToCollection($ruleCollection, $object);

			/* @var $rule DerModPro_GroupSwitcher_Model_Rule */

			// debug logging code ----------------------------------------------
			//$ruleCollection->load(false, true); // debug collection sql
			/*
			$debugTypes = array();
			foreach ($ruleCollection as $rule)
			{
				if (! isset($debugTypes[$rule->getRuleType()])) $debugTypes[$rule->getRuleType()] = 1;
				else $debugTypes[$rule->getRuleType()]++;
			}
			foreach ($debugTypes as $ruleType => $count)
			{
				Mage::helper('GroupSwitcher')->log(sprintf('Event %s: found %d rule(s) of type %s', $eventType, $count, $ruleType));
			}
			if (! $debugTypes) Mage::log('No matching rules found');
			 */
			// end debug code --------------------------------------------------
			
			foreach ($ruleCollection as $rule) {
			    /* @var $rule DerModPro_GroupSwitcher_Model_Rule */
				$ruleType = $rule->setEvent($eventType)->process($object);
				
				if ($ruleType->matchRule() && $rule->getStopProcessing()) {
				    // if the current rule was successfully applied and
				    // "Stop Processing" is set to "Yes", break loop
					break;
				}
			}
		}

		return $this;
	}

    /**
     *
     * @param string $eventType
     * @return array
     */
    protected function _getRuleTypesByEvent($eventType)
    {
        $ruleTypes = array();
        foreach (Mage::helper('GroupSwitcher')->getAllRuleTypes() as $ruleType => $info)
        {
            if (isset($info['events']) && array_key_exists($eventType, $info['events'])) $ruleTypes[] = $ruleType;
        }
        return $ruleTypes;
    }

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection $ruleCollection
	 * @param Varien_Object $object
	 * @return DerModPro_GroupSwitcher_Model_Observer
	 */
	protected function _addObjectFiltersToCollection(DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection $ruleCollection, Varien_Object $object)
	{
        if (isset($object))
        {
            if ($object instanceof Mage_Customer_Model_Customer)
            {
                $this->_addCustomerFiltersToCollection($ruleCollection, $object);
            }
            elseif ($object instanceof Mage_Sales_Model_Order)
            {
                $this->_addOrderFiltersToCollection($ruleCollection, $object);
            }

			/*
			 * In case some new module adds a new event type for the GroupSwitcher
			 */
			Mage::dispatchEvent('groupswitcher_rules_load_before', array('rules' => $ruleCollection, 'object' => $object));
        }

		return $this;
	}

    /**
     *
     * @param DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection $ruleCollection
     * @param Mage_Customer_Model_Customer $customer
     * @return DerModPro_GroupSwitcher_Model_Observer
     */
    protected function _addCustomerFiltersToCollection(DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection $ruleCollection, Mage_Customer_Model_Customer $customer)
    {
        if ($customer->getGroupId())
        {
            $ruleCollection->addGroupIdFilter($customer->getGroupId());
        }
        return $this;
    }

    /**
     *
     * @param DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection $ruleCollection
     * @param Mage_Sales_Model_Order $order
     * @return DerModPro_GroupSwitcher_Model_Observer 
     */
    protected function _addOrderFiltersToCollection(DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection $ruleCollection, Mage_Sales_Model_Order $order)
    {
        $ruleCollection
            ->addOrderStateFilter($order->getState())
			->addStoreIdFilter($order->getStoreId())
			;
        return $this;
    }

	public function scheduledGroupSwitches($schedule)
	{
		if (! Mage::helper('GroupSwitcher')->getConfig('enable_ext'))
		{
			/*
			 * Module deactivated for the global config scope
			 */
			return;
		}
		
		/*
		 * Special case: to enable processing of scheduled switches no rule
		 * needs to be defined at all. Calling process iterates over all
		 * current scheduled switches
		 */
		Mage::getModel('GroupSwitcher/schedule_switch')->process();
	}
}

