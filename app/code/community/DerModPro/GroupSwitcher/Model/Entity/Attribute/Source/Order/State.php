<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Entity_Attribute_Source_Order_State
	extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

	public function getAllOptions()
	{
		if (is_null($this->_options))
		{
			$this->_options = array(array(
				'value' => '',
				'label' => Mage::helper('GroupSwitcher')->__('Any'),
			));
			
			foreach (Mage::getConfig()->getNode('global/sales/order/states')->asArray() as $key => $value)
			{
				$this->_options[] = array(
					'value' => $key,
					'label' => Mage::helper('sales')->__($value['label']),
				);
			}
		}
		return $this->_options;
	}
}