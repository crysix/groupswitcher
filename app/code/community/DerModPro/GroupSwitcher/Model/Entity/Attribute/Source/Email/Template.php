<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Entity_Attribute_Source_Email_Template
	extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getAllOptions()
	{
		if (is_null($this->_options))
		{
			$templateConfigPath = Mage::helper('GroupSwitcher')->getConfigBasePath() . '/' . 'email_customer_template';
			
			$this->_options = Mage::getModel('adminhtml/system_config_source_email_template')
					/*
					 * For the "Default from locale" template option value
					 */
					->setPath($templateConfigPath)
					->toOptionArray();
		}
		return $this->_options;
	}
}