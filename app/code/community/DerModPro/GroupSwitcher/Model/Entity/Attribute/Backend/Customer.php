<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Entity_Attribute_Backend_Customer
	extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    public function beforeSave($object)
	{
		$attrCode = $this->getAttribute()->getAttributeCode();
		$data = trim($object->getData($attrCode));
		if ($data !== '')
		{
			$customer = Mage::getModel('customer/customer');
			if (is_numeric($data))
			{
				/*
				 * Lookup by ID
				 */
				$customer->load($data);
			}
			elseif (strpos($data, '@') !== false)
			{
				/*
				 * Lookup by Email
				 */
				if ($customer->getSharingConfig()->isWebsiteScope())
				{
					$customer->setWebsiteId($object->getCustomerWebsiteId());
				}
				$customer->loadByEmail($data);
			}
			else
			{
				$customer = false;
			}
			if (! $customer || ! $customer->getId())
			{
				Mage::throwException(
					Mage::helper('GroupSwitcher')->__('Unable to find customer identified by "%s"', Mage::helper('GroupSwitcher')->htmlEscape($data))
				);
			}
			$object->setData($attrCode, $customer->getId());
			$object->setCustomerWebsiteId($customer->getWebsiteId());
		}
		return parent::beforeSave($object);
	}
}
