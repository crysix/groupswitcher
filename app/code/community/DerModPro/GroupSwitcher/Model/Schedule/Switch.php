<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Schedule_Switch extends DerModPro_GroupSwitcher_Model_Rule
{
	protected $_eventPrefix = 'groupswitcher_schedule_switch';

	protected $_eventObject = 'switch';
	
	protected function _construct()
	{
		parent::_construct();
		$this->_init('GroupSwitcher/schedule_switch');
	}

	/**
	 *
	 * @return string
	 */
	public function getRuleType()
	{
		return 'schedule';
	}

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return DerModPro_GroupSwitcher_Model_Schedule_Switch 
	 */
	public function importData(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		$this->setRule($rule);
        foreach ($rule->getData() as $key => $value)
        {
            if (isset($value) && substr($key, 0, 9) == 'schedule_')
            {
				$this->setData(substr($key, 9), $value);
            }
        }
		
		$date = Mage::app()->getLocale()->date()->addDay($rule->getScheduleSwitchDays());
		/*
		 * Set target switch date and bypass backend model processing
		 */
		$this->setDate($date->toString('Y-M-d'))->setDateIsFormated(true);

		$this->setComment(
			/*
			 * Translation omitted on purpose
			 */
			sprintf('Created by %s Rule "%s" (ID %d)', $rule->getTypeModel()->getLabel(), $rule->getName(), $rule->getId())
		);

		return $this;
	}

	/**
	 *
	 * @param Mage_Customer_Model_Customer $customer
	 * @return DerModPro_GroupSwitcher_Model_Schedule_Switch 
	 */
	public function setCustomer(Mage_Customer_Model_Customer $customer)
	{
		$this->setData('customer', $customer)
			->setCustomerId($customer->getId());
		
		return $this;
	}

	/**
	 *
	 * @return Mage_Customer_Model_Customer
	 */
	public function getCustomer()
	{
		$customer = $this->getData('customer');
		if (is_null($customer) && $this->getCustomerId())
		{
			$customer = Mage::getModel('customer/customer')->load($this->getCustomerId());
			$this->setCustomer($customer);
		}
		return $customer;
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Schedule_Switch 
	 */
	public function validate()
	{
		if (! $this->getCustomerId())
		{
			$message = Mage::helper('GroupSwitcher')->__('No customer specified for scheduled switch');
			if ($this->getRule())
			{
				$message .= ' ' . Mage::helper('GroupSwitcher')->__('%s rule "%s" (%d)',
						$this->getRule()->getTypeModel()->getLabel(), $this->getRule()->getName(), $this->getRule()->getId()
				);
			}
			Mage::throwException($message);
		}

		if (! $this->getGroupIdAfter())
		{
			$message = Mage::helper('GroupSwitcher')->__('No target group specified for scheduled switch');
			if ($this->getRule())
			{
				$message .= ' ' . Mage::helper('GroupSwitcher')->__('%s rule "%s" (%d)',
						$this->getRule()->getTypeModel()->getLabel(), $this->getRule()->getName(), $this->getRule()->getId()
				);
			}
			Mage::throwException($message);
		}

		if (! $this->getDate())
		{
			$message = Mage::helper('GroupSwitcher')->__('No date specified for scheduled switch');
			if ($this->getRule())
			{
				$message .= ' ' . Mage::helper('GroupSwitcher')->__('%s rule "%s" (%d)',
						$this->getRule()->getTypeModel()->getLabel(), $this->getRule()->getName(), $this->getRule()->getId()
				);
			}
			Mage::throwException($message);
		}

		return $this;
	}

	/**
	 *
	 * @return bool
	 */
	public function isValid()
	{
		try
		{
			$this->validate();
			return true;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Schedule_Switch
	 */
	protected function _beforeSave()
	{
		/*
		 * Check values are positive or NULL
		 */
		$groupIdBefore = $this->getGroupIdBefore();
		if (isset($groupIdBefore) && ! $groupIdBefore)
		{
			$this->setGroupIdBefore(null);
		}

		/*
		 * On flat table entities the beforeSave() method on the attribute backend
		 * models is not called by default. We need that for the customer id and the
		 * date filed handlers
		 */
		foreach ($this->getAttributes() as $attribute)
		{
			if ($attribute->getBackend())
			{
				$attribute->getBackend()->beforeSave($this);
			}
		}
		
		return parent::_beforeSave();
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Schedule_Switch
	 */
	protected function _afterSave()
	{
		if ($this->getReplaceCustomerSwitches())
		{
			$this->getResource()->deleteAllOtherSwitchesForTheSameCustomer($this);
		}
		return parent::_afterSave();
	}
}

