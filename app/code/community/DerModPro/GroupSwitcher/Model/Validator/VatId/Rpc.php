<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Validator_VatId_Rpc
{
	protected $_validationUrl = 'http://evatr.bff-online.de/';

	protected $_successCode = 200;

	/**
	 * Simple test data array with vat id numbers I found via google.
	 *
	 * @var array
	 */
	protected $_testIds = array(
		//'DE182071978' => false,
		//'DK29225443' => true,
		'DK18408945' => true,
		'DK 18408945' => true,
		'GB-943866385' => true,
		'GB943866385' => true,
		'DE813708729' => false, // German numbers can't be validated using this service
		'GB123456' => false,
	);

	/**
	 *
	 * @param string $merchantVatId
	 * @param string $customerVatId
	 * @return bool
	 */
	public function isValid($merchantVatId, $customerVatId)
	{
		$result = $this->getResponse($merchantVatId, $customerVatId);
		
		$resultCode = $this->_getErrorCodeFromResult($result);
		
		return $resultCode == $this->_successCode;
	}

	/**
	 *
	 * @param string $result
	 * @return int
	 */
	protected function _getErrorCodeFromResult($result)
	{
		try
		{
			$xml = simplexml_load_string($result);
			/* @var $xml SimpleXMLElement */
			$node = $xml->xpath('//value[string="ErrorCode"]/../value[2]');
			$code = (string) $node[0]->string;
		}
		catch (Exception $e)
		{
			$code = 0;
		}
		return $code;
	}

	/**
	 *
	 * @param string $vatId
	 * @return string
	 */
	public function getCleanedVatId($vatId)
	{
		$vatId = preg_replace('/[^a-z0-9]/i', '', $vatId);
		return $vatId;
	}

	/**
	 * API Description: http://evatr.bff-online.de/eVatR/xmlrpc/
	 *
	 * @param string $merchantVatId
	 * @param string $customerVatId
	 * @return string
	 */
	public function getResponse($merchantVatId, $customerVatId)
	{
		$client = new Zend_XmlRpc_Client($this->_validationUrl);

		/*
		 * Dummy values, only simple validation supported
		 */
		$vatId1 = $this->getCleanedVatId($merchantVatId);
		$vatId2 = $this->getCleanedVatId($customerVatId);
		
		$company = '';
		$city = '';
		$zip = '';
		$street = '';
		$print = 'nein';

		$result = $client->call('evatrRPC', array($vatId1, $vatId2, $company, $city, $zip, $street, $print));
		return $result;
	}

	/**
	 * Run simple testcases
	 *
	 * @return bool
	 */
	public function test()
	{
		$allPassed = true;
		$merchantVatId = Mage::helper('GroupSwitcher')->getConfig('store_vat_id');
		foreach ($this->_testIds as $vatId => $expectedResult)
		{
			if (($result = $this->isValid($merchantVatId, $vatId)) != $expectedResult)
			{
				$allPassed = false;
				printf("[XML-RPC] Expected %s, got %s for ID \"%s\"\n",
					($expectedResult ? 'true' : 'false'), ($result ? 'true' : 'false'), $vatId
				);
			}
		}
		return $allPassed;
	}
}
