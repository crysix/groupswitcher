<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

/**
 * The vat ID number rules come from http://ec.europa.eu/taxation_customs/vies/faqvies.do#item11
 */
class DerModPro_GroupSwitcher_Model_Validator_VatId extends Varien_Object
{
	/**
	 * Regex Modifier String
	 *
	 * @var string
	 */
	protected $_rm = 'i';

	/**
	 * if this is false spaces re ignored in the passed vat id
	 *
	 * @var bool
	 */
	protected $_strictChecks = false;

	/**
	 * Simple test data array with valid vat id numbers
	 *
	 * @var array
	 */
	protected $_testIds = array(
		'ATU99999999' => true,
		'BE0999999999' => true,
		'BG999999999' => true,
		'BG9999999999' => true,
		'CY99999999L' => true,
		'CZ99999999' => true,
		'CZ999999999' => true,
		'CZ9999999999' => true,
		'DE999999999' => true,
		'DK99 99 99 99' => true,
		'EE999999999' => true,
		'EL999999999' => true,
		'ESX9999999X' => true,
		'FI99999999' => true,
		'FRXX 999999999' => true,
		'GB999 9999 99' => true,
		'GB999 9999 99 999' => true,
		'GBGD999' => true,
		'GBHA999' => true,
		'HU99999999' => true,
		'IE9S99999L' => true,
		'IT99999999999' => true,
		'LT999999999' => true,
		'LT999999999999' => true,
		'LU99999999' => true,
		'LV99999999999' => true,
		'MT99999999' => true,
		'NL999999999B99' => true,
		'PL9999999999' => true,
		'PT999999999' => true,
		'RO999999999' => true,
		'SE999999999999' => true,
		'SI99999999' => true,
		'SK9999999999' => true,
	);


	/**
	 * Check if the given VAT ID number is valid
	 *
	 * @param string $data
	 * @return bool
	 */
	public function isValidVatId($data)
	{
		$vatId = trim($data);
		if (strlen($vatId) < 2)
		{
			return false;
		}

		if (! $this->getStrictChecks() && strpos($vatId, ' ') !== false)
		{
			$vatId = str_replace(' ', '', $vatId);
		}
		
		$country = strtolower(substr($vatId, 0, 2));

		$method = $this->_camelize('validate_vat_id_' . $country);
		if (method_exists($this, $method))
		{
			return (bool) $this->{$method}($vatId);
		}
		return false;
	}

	/**
	 *
	 * @param bool $flag
	 * @return DerModPro_GroupSwitcher_Model_Validator_VatId
	 */
	public function setStrictChecks($flag)
	{
		$this->_strictChecks = (bool) $flag;
		return $this;
	}

	/**
	 *
	 * @return bool
	 */
	public function getStrictChecks()
	{
		return $this->_strictChecks;
	}

	/**
	 * Run a simple test, just check all the sample id's validate.
	 * This should be extended.
	 *
	 * @return boolean
	 */
	public function test()
	{
		$allPassed = true;
		foreach ($this->_testIds as $id => $expectedResult)
		{
			$this->setStrictChecks(true);
			if (($result = $this->isValidVatId($id)) != $expectedResult)
			{
				$allPassed = false;
				printf("[SYNTAX]  Expected %s, got %s for ID %s\n",
					($expectedResult ? 'true' : 'false'), ($result ? 'true' : 'false'), $id
				);
			}
			/*
			 * Test ID without spaces
			 */
			if (strpos($id, ' ') !== false)
			{
				$this->setStrictChecks(false);
				if (($result = $this->isValidVatId($id)) != $expectedResult)
				{
					$allPassed = false;
					printf("[SYNTAX]  Expected %s, got %s for ID %s\n",
						($expectedResult ? 'true' : 'false'), ($result ? 'true' : 'false'), str_replace(' ', '', $id)
					);
				}
			}
		}
		return $allPassed;
	}

	/**
	 * AT-Austria
	 * ATU999999991
	 * 1 block of 9 characters
	 * The 1st position following the prefix is always "U".
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdAt($vatId)
	{
		return preg_match('/^ATU[a-z0-9]{8}$/' . $this->_rm, $vatId);
	}

	/**
	 * BE-Belgium
	 * BE09999999992
	 * 1 block of 10 digits
	 * The first digit following the prefix is always zero ('0').
	 * The (new) 10-digit format is the result of adding a leading zero to the (old) 9-digit format.
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdBe($vatId)
	{
		return preg_match('/^BE0\d{9}$/' . $this->_rm, $vatId);
	}

	/**
	 * BG-Bulgaria
	 * BG999999999 or BG9999999999
	 * 1 block of 9 digits or 1 block of 10 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdBg($vatId)
	{
		return preg_match('/^BG\d{9,10}$/' . $this->_rm, $vatId);
	}

	/**
	 * CY-Cyprus
	 * CY99999999L
	 * 1 block of 9 characters
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdCy($vatId)
	{
		return preg_match('/^CY[a-z0-9]{9}$/' . $this->_rm, $vatId);
	}

	/**
	 * CZ-Czech Republic
	 * CZ99999999 or CZ999999999 or CZ9999999999
	 * 1 block of either 8, 9 or 10 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdCz($vatId)
	{
		return preg_match('/^CZ\d{8,10}$/' . $this->_rm, $vatId);
	}

	/**
	 * DE-Germany
	 * DE999999999
	 * 1 block of 9 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdDe($vatId)
	{
		return preg_match('/^DE\d{9}$/' . $this->_rm, $vatId);
	}

	/**
	 * DK-Denmark
	 * DK99 99 99 99
	 * 4 blocks of 2 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdDk($vatId)
	{
		return preg_match('/^DK\d\d ?\d\d ?\d\d ?\d\d$/' . $this->_rm, $vatId);
	}

	/**
	 * EE-Estonia
	 * EE999999999
	 * 1 block of 9 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdEe($vatId)
	{
		return preg_match('/^EE\d{9}$/' . $this->_rm, $vatId);
	}

	/**
	 * EL-Greece
	 * EL999999999
	 * 1 block of 9 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdEl($vatId)
	{
		return preg_match('/^EL\d{9}$/i', $vatId);
	}

	/**
	 * ES-Spain
	 * ESX9999999X4
	 * 1 block of 9 characters
	 * The first and last characters may be alpha or numeric; but they may not both be numeric.
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdEs($vatId)
	{
		if (preg_match('/^ES([a-z0-9])[a-z0-9]{7}([a-z0-9])$/i', $vatId, $matches))
		{
			$cmp = $matches[1] . $matches[2];
			if (! (preg_match('/\d{2}/', $cmp) || preg_match('/[a-z]{2}/', $cmp)))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * FI-Finland
	 * FI99999999
	 * 1 block of 8 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdFi($vatId)
	{
		return preg_match('/^FI\d{8}$/i', $vatId);
	}

	/**
	 * FR-France
	 * FRXX 999999999
	 * 1 block of 2 characters, 1 block of 9 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdFr($vatId)
	{
		return preg_match('/^FR[a-z]{2} ?\d{9}$/i', $vatId);
	}

	/**
	 * GB-United Kingdom
	 * GB999 9999 99 or GB999 9999 99 9995 or GBGD9996 or GBHA9997
	 * 1 block of 3 digits, 1 block of 4 digits and 1 block of 2 digits; or
	 * the above followed by a block of 3 digits; or
	 * 1 block of 5 characters.
	 * 
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdGb($vatId)
	{
		return preg_match('/^GB\d{3} ?\d{4} ?\d{2}$/' . $this->_rm, $vatId) ||
			preg_match('/^GB\d{3} ?\d{4} ?\d{2} ?\d{3}$/' . $this->_rm, $vatId) ||
			preg_match('/^GB[a-z0-9]{5}$/' . $this->_rm, $vatId);
	}

	/**
	 * HU-Hungary
	 * HU99999999
	 * 1 block of 8 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdHu($vatId)
	{
		return preg_match('/^HU\d{8}$/' . $this->_rm, $vatId);
	}

	/**
	 * IE-Ireland
	 * IE9S99999L
	 * 1 block of 8 characters
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdIe($vatId)
	{
		return preg_match('/^IE[0-9a-z]{8}$/' . $this->_rm, $vatId);
	}

	/**
	 * IT-Italy
	 * IT99999999999
	 * 1 block of 11 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdIt($vatId)
	{
		return preg_match('/^IT\d{11}$/' . $this->_rm, $vatId);
	}

	/**
	 * LT-Lithuania
	 * LT999999999 or LT999999999999
	 * 1 block of 9 digits, or 1 block of 12 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdLt($vatId)
	{
		return preg_match('/^LT(?:\d{9}|\d{12})$/' . $this->_rm, $vatId);
	}

	/**
	 * LU-Luxembourg
	 * LU99999999
	 * 1 block of 8 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdLu($vatId)
	{
		return preg_match('/^LU\d{8}$/' . $this->_rm, $vatId);
	}

	/**
	 * LV-Latvia
	 * LV99999999999
	 * 1 block of 11 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdLv($vatId)
	{
		return preg_match('/^LV\d{11}$/' . $this->_rm, $vatId);
	}

	/**
	 * MT-Malta
	 * MT99999999
	 * 1 block of 8 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdMt($vatId)
	{
		return preg_match('/^MT\d{8}$/' . $this->_rm, $vatId);
	}

	/**
	 * NL-The Netherlands
	 * NL999999999B998
	 * 1 block of 12 characters
	 * The 10th position following the prefix is always "B".
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdNl($vatId)
	{
		return preg_match('/^NL[a-z0-9]{9}B[a-z0-9]{2}$/' . $this->_rm, $vatId);
	}

	/**
	 * PL-Poland
	 * PL9999999999
	 * 1 block of 10 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdPl($vatId)
	{
		return preg_match('/^PL\d{10}$/' . $this->_rm, $vatId);
	}

	/**
	 * PT-Portugal
	 * PT999999999
	 * 1 block of 9 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdPt($vatId)
	{
		return preg_match('/^PT\d{9}$/' . $this->_rm, $vatId);
	}

	/**
	 * RO-Romania
	 * RO999999999
	 * 1 block of minimum 2 digits and maximum 10 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdRo($vatId)
	{
		return preg_match('/^RO\d{2,10}$/' . $this->_rm, $vatId);
	}

	/**
	 * SE-Sweden
	 * SE999999999999
	 * 1 block of 12 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdSe($vatId)
	{
		return preg_match('/^SE\d{12}$/' . $this->_rm, $vatId);
	}

	/**
	 * SI-Slovenia
	 * SI99999999
	 * 1 block of 8 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdSi($vatId)
	{
		return preg_match('/^SI\d{8}$/' . $this->_rm, $vatId);
	}

	/**
	 * SK-Slovakia
	 * SK9999999999
	 * 1 block of 10 digits
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function validateVatIdSk($vatId)
	{
		return preg_match('/^SK\d{10}$/' . $this->_rm, $vatId);
	}
}

