<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct()
	{
		$this->_init('GroupSwitcher/rule');
	}

	public function addGroupIdFilter($groupId)
	{
		if ($groupId)
		{
			$this->getSelect()
				->where("group_id_before = ? OR group_id_before IS NULL", $groupId)
				->where("group_id_after != ?", $groupId)
				;
		}
		return $this;
	}

	public function addOrderStateFilter($orderState)
	{
		if ($orderState)
		{
			$this->getSelect()
				->where("order_state = ? OR order_state IS NULL", $orderState)
				;
		}
		return $this;
	}

	public function addActiveFilter()
	{
		$this->getSelect()
			->where("is_active = 1")
			;
		return $this;
	}

	public function addStoreIdFilter($storeId)
	{
		if ($storeId)
		{
			$this->getSelect()
				->where("FIND_IN_SET(?, `store_ids`)", intval($storeId))
				;
		}
		return $this;
	}
	
	public function addEventTypeFilter($event)
	{
		if (! $event) return;
		
		$validTypes = array();
		foreach (Mage::helper('GroupSwitcher')->getAllRuleTypes() as $type => $data)
		{
			if (array_key_exists($event, $data['events'])) $validTypes[] = $type;
		}
		
		if (! $validTypes)
		{
			/*
			 * No triggers for this event defined
			 */
			$this->getSelect()->where('1 = 0');
		}
		else
		{
            $this->getSelect()->where("rule_type IN (%s)", $validTypes);
		}
		
		return $this;
	}

	public function addRuleTypeFilter($ruleType)
	{
		$this->getSelect()
			->where("rule_type IN (?)", $ruleType)
			;
		return $this;
	}
}
