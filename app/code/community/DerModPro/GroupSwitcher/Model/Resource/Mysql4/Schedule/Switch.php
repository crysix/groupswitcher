<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch extends DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule
{
	/**
	 * The entity type code that getAttributes() returns the attributes for
	 *
	 * @var string
	 */
	protected $_entityTypecode = 'groupswitcher_schedule_switch';

	/**
	 * Initialize table and id column
	 */
	protected function _construct()
	{
		$this->_init('GroupSwitcher/schedule_switch', 'entity_id');
	}

	/**
	 * Delete all other scheduled switches for the customer
	 *
	 * @param DerModPro_GroupSwitcher_Model_Schedule_Switch $switch
	 * @return DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch 
	 */
	public function deleteAllOtherSwitchesForTheSameCustomer(DerModPro_GroupSwitcher_Model_Schedule_Switch $switch)
	{
		if ($switch->getId() && $switch->getCustomerId())
		{
			$where = array(
				'entity_id != ?' => $switch->getId(),
				'customer_id = ?' => $switch->getCustomerId(),
			);
			$select = $this->_getWriteAdapter()->delete($this->getMainTable(), $where);
		}
		return $this;
	}
}