<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_System_Config_Backend_Vatid extends Mage_Core_Model_Config_Data
{
	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_System_Config_Backend_Vatid
	 */
	protected function  _beforeSave()
	{
		/*
		 * Only let valid german vat id numbers be configured
		 */
		$vatId = trim($this->getValue());
		if ($vatId !== '')
		{
			if (! Mage::helper('GroupSwitcher')->isVatValid($vatId))
			{
				$message = Mage::helper('GroupSwitcher')->__('Invaid Vat ID Number: "%s"', $vatId);
				Mage::getSingleton('adminhtml/session')->addError($message);
				$vatId = '';
			}
		}
		$this->setValue($vatId);
		return parent::_beforeSave();
	}
}

