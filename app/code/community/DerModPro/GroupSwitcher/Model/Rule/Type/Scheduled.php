<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

/**
 * Trigger pending scheduled switches
 */
class DerModPro_GroupSwitcher_Model_Rule_Type_Scheduled extends DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
{
	/**
	 * Trigger pending scheduled switches
	 *
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Scheduled
	 */
	protected function _process()
	{
		$switches = Mage::getResourceModel('GroupSwitcher/schedule_switch_collection');
		/* @var $switches DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch_Collection */
		$switches
			//->addFieldToFilter('date', Mage::app()->getLocale()->date()->toString('Y-M-d'))
			->addDateFilter()
			->setOrder('entity_id', Varien_Data_Collection_Db::SORT_ORDER_ASC)
		;
		
		/* @var $rule DerModPro_GroupSwitcher_Model_Schedule_Switch */
		foreach ($switches as $rule)
		{
			$customer = $rule->getCustomer();
			if (! $customer->getId())
			{
				/*
				 * Customer doesn't exist (anymore)?
				 * Foreign key constraints should have taken care of this case.
				 * Anyway, why not take care of it just to be on the safe side.
				 */
				continue;
			}

			$rule->setTypeModel($this);
			$this->setRule($rule)->_switchCustomerGroup($customer);

			/*
			 * Update rule comment
			 */
			$comment = $rule->getComment() . "\n" . $this->_getRuleProcessedComment();
			$rule->setComment($comment)
					/*
					 * Bypass backend model processing for date
					 */
					->setDateIsFormated(true)
					->save();
		}
		
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	protected function _getRuleProcessedComment()
	{
		$message = Mage::helper('GroupSwitcher')->__(
			'Rule processed at %s', Mage::app()->getLocale()->date()->toString('Y-M-d H:m:s')
		);
		return $message;
	}
}

