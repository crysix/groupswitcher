<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */
/**
 * @see DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
 */
interface DerModPro_GroupSwitcher_Model_Rule_Type_Interface
{
	public function setRule(DerModPro_GroupSwitcher_Model_Rule $rule);
	public function getRule();
	public function setObject(Varien_Object $object = null);
	public function getObject();
	public function process(Varien_Object $object = null);
	public function getBlockClass();
	public function getLabel();
	public function processRuleBeforeSave(DerModPro_GroupSwitcher_Model_Rule $rule);
	public function isAvailable();
}