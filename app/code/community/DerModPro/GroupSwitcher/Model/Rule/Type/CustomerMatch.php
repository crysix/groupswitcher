<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule_Type_CustomerMatch extends DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
{
	const MATCH_TYPE_PLAIN = 'plain';
	const MATCH_TYPE_REGEX = 'regex';
	const MATCH_TYPE_CALLBACK = 'callback';

    /**
     * Check if the attribute rule value matches the customers attribute
     *
     * @return bool
     */
	public function matchRule()
	{
		if (parent::matchRule())
		{
			$attribute = $this->getRule()->getRuleValue1();
			$match = $this->getRule()->getRuleValue2();
			$matchType = $this->getRule()->getRuleValue3();

			$customerAttributeValue = $this->_getCustomer()->getDataUsingMethod($attribute);

			//Mage::helper('GroupSwitcher')->log(sprintf('Matching "%s" against pattern "%s" with method %s', $customerAttributeValue, $match, $matchType));

			switch ($matchType)
			{
				case self::MATCH_TYPE_PLAIN:
					return $match == $customerAttributeValue;

				case self::MATCH_TYPE_REGEX:
					try
					{
						return preg_match($match, $customerAttributeValue);
					}
					catch (Exception $e)
					{
						$message = Mage::helper('GroupSwitcher')->__('Invalid regular expression "%s" for %s rule "%s" (ID %d)',
							$match, $this->getLabel(), $this->getRule()->getName(), $this->getRule()->getId()
						);
						$this->_logRuleException($message);
						return false;
					}

				case self::MATCH_TYPE_CALLBACK:
					return $this->_handleModelCallback($match, $attribute);
			}
		}

		return false;
	}

	/**
	 * The callback needs to be specified in the format
	 *    module/model::method
	 *
	 * @param string $match
	 * @param string $callback Format: module/model::method
	 * @param string $attributeCode
	 * @return bool
	 */
	protected function _handleModelCallback($callback, $attributeCode)
	{
		@list($model, $method) = explode('::', $callback);
		if (isset($model) && isset($method))
		{
			try
			{
				$instance = Mage::getModel($model);
				if (! $instance)
				{
					Mage::throwException(
						Mage::helper('GroupSwitcher')->__('Unable to create instance of model %s', $model)
					);
				}
				return $instance->{$method}($this->_getCustomer(), $attributeCode, $this->getRule());
			}
			catch(Exception $e)
			{
				$message = Mage::helper('GroupSwitcher')->__('Invalid callback "%s" for %s rule "%s" (%d). %s',
					$callback, $this->getLabel(), $this->getRule()->getName(), $this->getRule()->getId(), $e->getMessage()
				);
				$this->_logRuleException($message);
				return false;
			}
		}
	}

	/**
	 *
	 * @return array
	 */
	public function getMatchTypeOptions()
	{
		$options = array(
			self::MATCH_TYPE_PLAIN    => Mage::helper('GroupSwitcher')->__('Plain Value'),
			self::MATCH_TYPE_REGEX    => Mage::helper('GroupSwitcher')->__('Regular Expression'),
			self::MATCH_TYPE_CALLBACK => Mage::helper('GroupSwitcher')->__('Model Callback'),
		);
		return $options;
	}

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_CustomerMatch
	 */
	public function processRuleBeforeSave(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		$match = $this->getRule()->getRuleValue2();
		$matchType = $this->getRule()->getRuleValue3();

		if ($matchType == self::MATCH_TYPE_REGEX)
		{
			/*
			 * Test regex
			 */
			try
			{
				preg_match($match, 'dummy testvalue');
			}
			catch (Exception $e)
			{
				Mage::throwException(
					Mage::helper('GroupSwitcher')->__('Invalid regular expression: "%s"', $match)
				);
			}
		}
		elseif ($matchType == self::MATCH_TYPE_CALLBACK)
		{
			$checkPassed = false;
			@list($model, $method) = explode('::', $match);
			if (isset($model) || isset($method))
			{
				try
				{
					$instance = false;
					$instance = Mage::getModel($model);
				}
				catch (Exception $e) {}
				if ($instance)
				{
					if (is_callable(array($instance, $method)))
					{
						$checkPassed = true;
					}
				}
			}
			if (! $checkPassed)
			{
				Mage::throwException(
					Mage::helper('GroupSwitcher')->__('Invalid model callback: "%s"', $match)
				);
			}
		}

		return parent::processRuleBeforeSave($rule);
	}
}

