<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule_Type_NumOrders extends DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
{
    /**
     * Check the number of orders matching the order state agains the comparison value
     *
     * @return bool
     */
	public function matchRule()
	{
		if (parent::matchRule())
		{
            
			$comparison = $this->getRule()->getRuleValue1();
			$ruleNum = $this->getRule()->getRuleValue2();

			$orderNum = $this->_getCustomerOrderNum();

			switch ($comparison)
			{
				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_GREATER_OR_EQUAL:
					return $orderNum >= $ruleNum;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_GREATER:
					return $orderNum > $ruleNum;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_LESS_OR_EQUAL:
					return $orderNum <= $ruleNum;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_LESS:
					return $orderNum < $ruleNum;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_EQUAL:
					return $orderNum == $ruleNum;
			}
		}

		return false;
	}

	/**
	 *
	 * @return int
	 */
	protected function _getCustomerOrderNum()
	{
		$customer = $this->_getCustomer();

		$collection = Mage::getResourceModel('sales/order_grid_collection')
			->addFieldToFilter('customer_id', $customer->getId())
		;

		if ($state = $this->getRule()->getOrderState())
		{
			$statuses = $this->_getOrderConfig()->getStateStatuses($state, false);
			$collection->addFieldToFilter('status', array('in' => $statuses));
		}

		/*
		 * rule_value3 = "the last N days"
		 */
		if ($timeframe = $this->getRule()->getRuleValue3())
		{
			$date = Mage::app()->getLocale()->date()->subDay($timeframe);
			$collection->addFieldToFilter('created_at', array('gt' => $date->toString('Y-M-d')));
		}

		$size = $collection->getSize();
		//Mage::helper('GroupSwitcher')->log(sprintf('Orders in state "%s" within the last "%s" days: %d', $state, $timeframe, $size));
		
		return $size;
	}

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_NumOrders
	 */
	public function processRuleBeforeSave(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		/*
		 * Set a integer value for the last N days
		 */
		if (! is_numeric($rule->getRuleValue3()))
		{
			$rule->setRuleValue3(intval($rule->getRuleValue3()));
		}
		
		return parent::processRuleBeforeSave($rule);
	}

	/**
	 *
	 * @return Mage_Sales_Model_Order_Config
	 */
	protected function _getOrderConfig()
	{
		return Mage::getSingleton('sales/order_config');
	}
    
    /**
     * Because this rule type is triggered with an order update or with login
     * events we need to get the order in two different ways, depending on the
     * trigger event.
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _getOrder()
    {
        if ($this->getObject() instanceof Mage_Sales_Model_Order)
        {
            /*
             * Triggered by order update
             */
            return $this->getObject();
        }

        /*
         * Triggered by login
         * Return a dummy order model
         */
        $order = $this->getData('order');
        if (is_null($order))
        {
            /*
             * Fake order model
             */
            $order = Mage::getModel('sales/order')
                ->setCustomer($this->_getCustomer())
                ->setCustomerId($this->_getCustomer()->getId());
            $this->setOrder($order);
        }
        return $order;
    }

    /**
     * Because this rule type is triggered with an order update or with login
     * events we need to get the customer in two different ways, depending on
     * the trigger event.
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomer()
    {
        if ($this->getObject() instanceof Mage_Customer_Model_Customer)
        {
            /*
             * Triggered by login
             */
            return $this->getObject();
        }

        /**
         * Triggered by order update
         */
        $customer = $this->_getOrder()->getCustomer();
        if (! $customer)
        {
            $customer = Mage::getModel('customer/customer')->load($this->_getOrder()->getCustomerId());
            $this->_getOrder()->setCustomer($customer);
        }
        return $customer;
    }
}

