<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule_Type_OrderTotal extends DerModPro_GroupSwitcher_Model_Rule_Type_Order_Abstract
{
    /**
     * Check the order total against the rule value
     *
     * @return bool
     */
	public function matchRule()
	{
		if (parent::matchRule())
		{
			$comparison = $this->getRule()->getRuleValue1();
			$ruleTotal = $this->getRule()->getRuleValue2();

			$total = $this->_getOrder()->getBaseSubtotal();
			
			switch ($comparison)
			{
				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_GREATER_OR_EQUAL:
					return $total >= $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_GREATER:
					return $total > $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_LESS_OR_EQUAL:
					return $total <= $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_LESS:
					return $total < $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_EQUAL:
					return $total == $ruleTotal;
			}
		}

		return false;
	}
}
