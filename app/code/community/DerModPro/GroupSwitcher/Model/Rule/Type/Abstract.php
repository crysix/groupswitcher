<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

/**
 * Abstract groupswitcher rule type
 *
 * Rule processing goes as follows:
 *
 * 1) The DerModPro_GroupSwitcher_Model_Rule instance containing the configuration
 *    for the current rule is set on this rule type model.
 *
 * 2) process() is called, which in turn calls...
 *
 * 3) _isValidObject() to check if the passed object is valid for the
 *    rule type (regardless of the rule configuration settings)
 *
 * 4) matchRule() is called to check if the passed object is valid for the rule
 *    configuration settings
 * 
 * 5) if valid and matched, _process() is called to apply any group switches
 * 
 * 6) if valid and matched, _process() needs to call _scheduleSwitch() and pass
 *    a customer model to save a scheduled group switch rule if configured
 *
 */
abstract class DerModPro_GroupSwitcher_Model_Rule_Type_Abstract extends Varien_Object
	implements DerModPro_GroupSwitcher_Model_Rule_Type_Interface
{
	const XML_PATH_EMAIL_IDENTITY = 'dermodpro_groupswitcher/general/email_identity';

    /**
     * The instance of the rule with the configuration values
     *
     * @var DerModPro_GroupSwitcher_Model_Rule
     * @see DerModPro_GroupSwitcher_Model_Rule_Type_Abstract::setRule()
     * @see DerModPro_GroupSwitcher_Model_Rule_Type_Abstract::getRule()
     */
	protected $_rule;

    /**
     * The object passed as a parameter to to process()
     *
     * @var Varien_Object | null
     * @see DerModPro_GroupSwitcher_Model_Rule_Type_Abstract::setObject()
     * @see DerModPro_GroupSwitcher_Model_Rule_Type_Abstract::getObject()
     */
    protected $_object;

	/**
	 * Enforce type of rule object
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule_Type_Abstract $rule
	 */
	public function setRule(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		$this->_rule = $rule;
		return $this;
	}

    /**
	 * Return the rule instance with all configuration values for this rule
     *
     * @return DerModPro_GroupSwitcher_Model_Rule
     */
	public function getRule()
	{
		return $this->_rule;
	}

    /**
     *
     * @param Varien_Object $object
     * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
     */
    public function setObject(Varien_Object $object = null)
    {
        $this->_object = $object;
        return $this;
    }

    /**
     *
     * @return Varien_Object | null
     */
    public function getObject()
    {
        return $this->_object;
    }

	/**
	 * Make some basic validity checks before passing the flow of controll to the specific rule logic
	 *
	 * @param Varien_Object $object
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
	 */
	final public function process(Varien_Object $object = null)
	{
        $this->setObject($object);
        
		if ((is_null($object) || $this->_isValidObject()) && $this->matchRule())
        {
            $this->_process();
        }
        
        return $this;
	}

    /**
     * Check if the rule or should be applied or switches should be scheduled.
     *
     * @return bool
     */
    public function matchRule()
    {
        if (! $this->_checkCustomerGroupBefore($this->_getCustomer())) {
            return false;
        }
        
        return true;
    }

    /**
     * Apply any rules that should take effect now
     *
     * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
     */
	protected function _process()
	{
		if ($customer = $this->_getCustomer())
		{
			if ($targetGroupId = $this->getRule()->getGroupIdAfter())
			{
				if ($customer->getGroupId() != $targetGroupId)
				{
					$this->_switchCustomerGroup($customer);
				}
			}

			$this->_scheduleSwitch($customer);
		}

		return $this;
	}

	/**
	 * Return customer object if appropriate for rule type.
	 * Override as needed...
	 *
	 * @return Mage_Customer_Model_Customer
	 */
	protected function _getCustomer()
	{
		return $this->getObject();
	}

    /**
     * Schedule any future customer group switches
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
     */
    protected function _scheduleSwitch(Mage_Customer_Model_Customer $customer)
    {
		if ($this->getRule()->getScheduleSwitch() && $this->getRule()->getScheduleSwitchDays() > 0)
		{
			try
			{
				$log = sprintf(
					'Scheduling group switch for customer %s from group %s to group %d in %d day(s)',
						$customer->getId(),
						(($g = $this->getRule()->getScheduleGroupIdBefore()) ? $g : 'any'),
						$this->getRule()->getScheduleGroupIdAfter(),
						$this->getRule()->getScheduleSwitchDays()
				);
				if ($this->getRule()->getScheduleReplaceCustomerSw())
				{
					$log .= ' (replacing all existing scheduled switches for the customer)';
				}
				Mage::helper('GroupSwitcher')->log($log);

				$scheduledSwitch = $this->_getScheduleSwitchModel()
					->setCustomer($customer)
					->importData($this->getRule())
					->validate()
					->save();
			}
			catch (Exception $e)
			{
				Mage::logException($e);
			}
		}

        return $this;
    }

    /**
     *
     * @return DerModPro_GroupSwitcher_Model_Schedule_Switch
     */
    protected function _getScheduleSwitchModel()
    {
        return Mage::getModel('GroupSwitcher/schedule_switch');
    }

    /**
     *
     * @param Mage_Customer_Model_Customer $customer
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
     */
    protected function _switchCustomerGroup(Mage_Customer_Model_Customer $customer)
    {
		if ($this->_checkCustomerGroupIsDifferentFromGroupIdAfter($customer))
        {
			Mage::helper('GroupSwitcher')->log("Rule \"{$this->getRule()->getName()}\" (type {$this->getRule()->getRuleType()}, ID {$this->getRule()->getId()}): Setting group {$this->getRule()->getGroupIdAfter()} for customer {$customer->getName()} (ID {$customer->getId()})");
            $customer->setGroupId($this->getRule()->getGroupIdAfter())
					/*
					 * Set flag to avoid triggering the customer save event again.
					 * It is a pitty getResource()->saveAttribute() doesn't work for static entity attributes :-(
					 */
					->setSkipGroupswitcherEvents(true)
					->save();
			

            /*
             * Also update value in session if this is a login session
             */
            if (($session = Mage::getSingleton('customer/session')) && $session->isLoggedIn())
            {
                if ($session->getCustomer()->getId() === $customer->getId())
                {
					if ($session->getCustomer()->getGroupId() != $this->getRule()->getGroupIdAfter())
					{
						$session->getCustomer()->setGroupId($this->getRule()->getGroupIdAfter());
					}
                }
            }

			if ($this->getRule()->getSendEmail())
			{
				$this->_sendGroupChangeNotification($customer, $this->getRule()->getEmailTemplate());
			}
        }

        return $this;
    }

	/**
	 * Return the block type for the block class.
	 *
	 * @return string
	 */
	public function getBlockClass()
	{
		$blockClass = (string) Mage::getConfig()->getNode(DerModPro_GroupSwitcher_Helper_Data::XML_RULE_TYPES_PATH . '/' . $this->getRule()->getRuleType() . '/block');
		return $blockClass;
	}

	/**
	 * Return the rule label from the configuration
	 *
	 * @return string
	 */
	public function getLabel($translate = true)
	{
		$label = (string) Mage::getConfig()->getNode(DerModPro_GroupSwitcher_Helper_Data::XML_RULE_TYPES_PATH . '/' . $this->getRule()->getRuleType() . '/label');
		if ($translate)
		{
			$label = Mage::helper('GroupSwitcher')->__($label);
		}
		return $label;
	}

	/**
	 *
	 * @param Mage_Customer_Model_Customer $customer
	 * @param int $template
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
	 */
	protected function _sendGroupChangeNotification(Mage_Customer_Model_Customer $customer, $template)
	{
		$translate = Mage::getSingleton('core/translate');
		/* @var $translate Mage_Core_Model_Translate */
		$translate->setTranslateInline(false);

		$mailTemplate = Mage::getModel('core/email_template');
		/* @var $mailTemplate Mage_Core_Model_Email_Template */

		$sendTo = array(
			array(
				'email' => $customer->getEmail(),
				'name'  => $customer->getName(),
			)
		);

		$newGroup = Mage::getModel('customer/group')->load($customer->getGroupId());
		$prevGroup = Mage::getModel('customer/group')->load($customer->getOrigData('group_id'));

		foreach ($sendTo as $recipient) {
			$mailTemplate->setDesignConfig(array('area'=>'frontend', 'store' => $customer->getStoreId()))
				->sendTransactional(
					$template,
					Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $customer->getStoreId()),
					$recipient['email'],
					$recipient['name'],
					array(
						'rule'      => $this->getRule(),
						'rule_type' => $this->getRule()->getTypeModel(),
						'customer'  => $customer,
						'new_group' => $newGroup,
						'prev_group' => $prevGroup,
					)
				);
			//Mage::helper('GroupSwitcher')->log("Sent notification email template {$template} to {$recipient['email']}");
		}
		$translate->setTranslateInline(true);

		return $this;
	}

	/**
	 *
	 * @return string
	 */
	protected function _getErrorLogFile()
	{
		return Mage::getStoreConfig('dev/log/exception_file');
	}

	/**
	 *
	 * @param string $message
	 * @param int $logLevel
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract 
	 */
	protected function _logRuleException($message, $logLevel = null)
	{
		if (is_null($logLevel))
		{
			$logLevel = Zend_Log::ERR;
		}
		Mage::helper('GroupSwitcher')->log($message, $logLevel);

		return $this;
	}

	/***************************************************************************
     * Validity checks that may be used in the specific rule classes
     **************************************************************************/

    /**
     * Check the passed object is valid to the current rule
     *
     * @return bool
     */

    protected function _isValidObject()
    {
        return true;
    }

	/**
	 * Check a order entity is loaded
	 *
     * @param Mage_Sales_Model_Order $order
	 * @return bool
	 */
	protected function _isValidOrder($order = null)
	{
		if (is_null($order))
		{
			$order = $this->getObject();
		}
		
        if (! $order instanceof Mage_Sales_Model_Order) return false;

        if (! $order->getCustomerId()) return false;

		return true;
	}

	/**
	 * Check the orders status matches the rules condition
	 *
	 * @return bool
	 */
	protected function _checkOrderState()
	{
        $order = $this->getObject();
		//Mage::helper('GroupSwitcher')->log('order state: ' . $order->getState() .  ', rule state: ' .  $this->getRule()->getOrderState());
		if (! $this->getRule()->getOrderState()) return true;
		return $this->getRule()->getOrderState() == $order->getState();
	}

	/**
	 * Check a customer entity is loaded
	 *
     * @param Mage_Customer_Model_Customer $customer
	 * @return bool
	 */
	protected function _isValidCustomer($customer = null)
	{
        if (is_null($customer))
        {
            $customer = $this->getObject();
        }
        if (! $customer instanceof Mage_Customer_Model_Customer) return false;
        
		return (bool) $customer->getId();
	}

	/**
	 * Check the customers group matches the rules condition
	 *
     * @param Mage_Customer_Model_Customer $customer
	 * @return bool
	 */
	protected function _checkCustomerGroupBefore(Mage_Customer_Model_Customer $customer = null)
	{
        if (is_null($customer))
        {
            $customer = $this->getObject();
        }
		//Mage::helper('GroupSwitcher')->log('customer group id: ' . $customer->getGroupId() . ', rule group id before: ' . $this->getRule()->getGroupIdBefore());
		if (! $this->getRule()->getGroupIdBefore()) return true;
		return $this->getRule()->getGroupIdBefore() == $customer->getGroupId();
	}

	/**
	 * Check the customers group is different from the rules target customer group
	 *
     * @param Mage_Customer_Model_Customer $customer
	 * @return bool
	 */
	protected function _checkCustomerGroupIsDifferentFromGroupIdAfter(Mage_Customer_Model_Customer $customer = null)
	{
        if (is_null($customer))
        {
            $customer = $this->getObject();
        }
		return $this->getRule()->getGroupIdAfter() != $customer->getGroupId();
	}

	/**
	 * Throw exception if the rule values do not match the required bounds.
	 * Option to set default values or any other rule type specific values on
	 * the rule model.
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
	 * @throws Mage_Core_Exception
	 */
	public function processRuleBeforeSave(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		return $this;
	}

	/**
	 *
	 * @return bool
	 */
	public function isAvailable()
	{
		return true;
	}
}

