<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

$this->run("
DROP TABLE IF EXISTS `{$this->getTable('GroupSwitcher/rule')}`;
CREATE TABLE `{$this->getTable('GroupSwitcher/rule')}` (
	`entity_id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`rule_type` VARCHAR(60) NOT NULL,
	`is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`priority` INT(10) NOT NULL DEFAULT 0,
	`name` VARCHAR(255) NOT NULL DEFAULT '',
    `store_ids` VARCHAR(255) NOT NULL DEFAULT '',
	`stop_processing` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`group_id_before` SMALLINT(3) UNSIGNED NULL,
	`group_id_after` SMALLINT(3) UNSIGNED NOT NULL,
    `order_state` VARCHAR(255) NULL,
    `rule_value1` VARCHAR(255) NOT NULL,
    `rule_value2` VARCHAR(255) NOT NULL,
    `rule_value3` VARCHAR(255) NOT NULL,
	`rule_config` TEXT NOT NULL DEFAULT '',
	`comment` VARCHAR(255) NOT NULL DEFAULT '',
    `email_template` VARCHAR(255) NULL,
    `send_email` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`created_at` DATETIME,
	`updated_at` DATETIME,

	`schedule_switch` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `schedule_switch_days` SMALLINT(3) UNSIGNED NULL,
    `schedule_group_id_before` SMALLINT(3) UNSIGNED NULL,
    `schedule_group_id_after` SMALLINT(3) UNSIGNED NULL,
    `schedule_replace_customer_sw` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `schedule_email_template` VARCHAR(255) NULL,
    `schedule_send_email` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,

	CONSTRAINT `FK_gprs_group_id_before` FOREIGN KEY (`group_id_before`) REFERENCES {$this->getTable('customer_group')} (`customer_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT `FK_gprs_group_id_after` FOREIGN KEY (`group_id_after`) REFERENCES {$this->getTable('customer_group')} (`customer_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	INDEX `IDX_grps_rules` (`rule_type`),
    INDEX `IDX_grps_active` (`is_active`),
    INDEX `IDX_grps_grp_before` (`group_id_before`),
    INDEX `IDX_grps_prio` (`priority`),
    INDEX `IDX_grps_order_state` (`order_state`),
    INDEX `IDX_grps_rule_values` (`rule_value1`, `rule_value2`, `rule_value3`),
	UNIQUE INDEX `UIDX_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{$this->getTable('GroupSwitcher/schedule_switch')}`;
CREATE TABLE `{$this->getTable('GroupSwitcher/schedule_switch')}` (
	`entity_id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `customer_id`INT(10) UNSIGNED NOT NULL,
    `customer_website_id`SMALLINT(5) UNSIGNED NULL,
	`group_id_before` SMALLINT(3) UNSIGNED NULL,
	`group_id_after` SMALLINT(3) UNSIGNED NOT NULL,
    `email_template` VARCHAR(255) NULL,
    `send_email` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `date` DATE NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`comment` VARCHAR(255) NOT NULL DEFAULT '',

	CONSTRAINT `FK_cron_customerid` FOREIGN KEY (`customer_id`) REFERENCES {$this->getTable('customer_entity')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `FK_cron_group_id_before` FOREIGN KEY (`group_id_before`) REFERENCES {$this->getTable('customer_group')} (`customer_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT `FK_cron_group_id_after` FOREIGN KEY (`group_id_after`) REFERENCES {$this->getTable('customer_group')} (`customer_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	INDEX `IDX_grps_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

/*
 * Install Entity Types
 */
$this->addEntityType('groupswitcher_rule', array(
		'entity_model' => 'GroupSwitcher/rule',
		'table' => 'GroupSwitcher/rule',
	)
);

$this->addEntityType('groupswitcher_schedule_switch', array(
		'entity_model' => 'GroupSwitcher/schedule_switch',
		'table' => 'GroupSwitcher/schedule_switch',
	)
);

/*
 * Install Attribute Groups
 */
$this->addAttributeGroup('groupswitcher_rule', 'Default', 'Rule Type', 10);
$this->addAttributeGroup('groupswitcher_rule', 'Default', 'Schedule Switch', 20);




foreach (array(
	/*
	 * Rule Entity Type
	 */
	'groupswitcher_rule' => array(
			
		/*
		 * General Configuration Attributes
		 */
		'rule_type' => array(
			'type'     => 'static',
			'label'    => 'Rule Type',
			'input'    => 'label',
			'required' => 0,
		),
		'name' => array(
			'type'     => 'static',
			'label'    => 'Name',
			'required' => 1,
		),
		'group_id_before' => array(
			'type'     => 'static',
			'label'    => 'Switch from Group',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_customer_group_any',
			'required' => 0,
		),
		'group_id_after' => array(
			'type'     => 'static',
			'label'    => 'Switch to Group',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_customer_group_none',
			'required' => 0,
		),
		'is_active' => array(
			'type'     => 'static',
			'label'    => 'Is Active',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'required' => 1,
		),
		'priority' => array(
			'type'     => 'static',
			'label'    => 'Priority',
			'required' => 0,
		),
		'stop_processing' => array(
			'type'     => 'static',
			'label'    => 'Stop Processing',
			'note'     => 'If set to yes further rules will be ignored',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'required' => 0,
		),
		'send_email' => array(
			'type'     => 'static',
			'label'    => 'Send customer email when setting new group',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'required' => 0,
		),
		'email_template' => array(
			'type'     => 'static',
			'label'    => 'Email Template',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_email_template',
			'required' => 0,
		),
		'comment' => array(
			'type'     => 'static',
			'label'    => 'Comment',
			'input'    => 'textarea',
			'required' => 0,
		),
		'created_at' => array(
			'type'     => 'static',
			'label'    => 'Created At',
			'input'    => 'label',
			'required' => 0,
		),
		'updated_at' => array(
			'type'     => 'static',
			'label'    => 'Updated At',
			'input'    => 'label',
			'required' => 0,
		),

		/*
		 * Rule Type Configuration Attributes
		 */
		'store_ids' => array(
			'type'     => 'static',
			'label'    => 'Store IDs',
			'input'    => 'multiselect',
			'source'   => 'eav/entity_attribute_source_store',
			'group'    => 'Rule Type',
			'required' => 0,
		),
		'order_state' => array(
			'type'     => 'static',
			'label'    => 'Order State',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_order_state',
			'note'     => 'Apply rule if order state matches',
			'group'    => 'Rule Type',
			'required' => 0,
		),
		'rule_value1' => array(
			'type'     => 'static',
			'label'    => 'Rule Value 1',
			'group'    => 'Rule Type',
			'required' => 0,
		),
		'rule_value2' => array(
			'type'     => 'static',
			'label'    => 'Rule Value 2',
			'group'    => 'Rule Type',
			'required' => 0,
		),
		'rule_value3' => array(
			'type'     => 'static',
			'label'    => 'Rule Value 3',
			'group'    => 'Rule Type',
			'required' => 0,
		),
		'rule_config' => array(
			'type'     => 'static',
			'label'    => 'Rule Configuration',
			'input'    => 'textarea',
			'note'     => 'Serialized PHP',
			'group'    => 'Rule Type',
			'required' => 0,
		),

		/*
		 * Scheduled Switch Configuration Attributes
		 */
		'schedule_switch' => array(
			'type'     => 'static',
			'label'    => 'Schedule a group switch',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'group'    => 'Schedule Switch',
			'required' => 1,
		),
		'schedule_switch_days' => array(
			'type'     => 'static',
			'label'    => 'Schedule Switch in N days',
			'group'    => 'Schedule Switch',
			'required' => 0,
		),
		'schedule_group_id_before' => array(
			'type'     => 'static',
			'label'    => 'Schedule Switch: from Group',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_customer_group_any',
			'group'    => 'Schedule Switch',
			'required' => 0,
		),
		'schedule_group_id_after'  => array(
			'type'     => 'static',
			'label'    => 'Schedule Switch: to Group',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_customer_group',
			'group'    => 'Schedule Switch',
			'required' => 0,
		),
		'schedule_replace_customer_sw' => array(
			'type'     => 'static',
			'label'    => 'Replace other scheduled switches for the customer',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'group'    => 'Schedule Switch',
			'required' => 0,
		),
		'schedule_send_email' => array(
			'type'     => 'static',
			'label'    => 'Send customer email when setting new group',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'group'    => 'Schedule Switch',
			'required' => 0,
		),
		'schedule_email_template' => array(
			'type'     => 'static',
			'label'    => 'Email Template',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_email_template',
			'group'    => 'Schedule Switch',
			'required' => 0,
		),
	),
	/*
	 * Schedule Switch Entity Type
	 */
	'groupswitcher_schedule_switch'  => array(
		'customer_id'  => array(
			'type'     => 'static',
			'label'    => 'Customer',
			'note'     => 'Customer ID or Email Address',
			'backend'  => 'GroupSwitcher/entity_attribute_backend_customer',
			'input'    => 'text',
			'required' => 1,
		),
		'customer_website_id' => array(
			'type'     => 'static',
			'label'    => 'Website ID',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_website',
			'required' => 1,
		),
		'date' => array(
			'type'     => 'static',
			'label'    => 'Switch at',
			'input'    => 'date',
			'backend'  => 'eav/entity_attribute_backend_datetime',
			'required' => 1,
		),
		'group_id_before' => array(
			'type'     => 'static',
			'label'    => 'Switch from Group',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_customer_group_any',
			'required' => 0,
		),
		'group_id_after' => array(
			'type'     => 'static',
			'label'    => 'Switch to Group',
			'input'    => 'select',
			'source'   => 'customer/customer_attribute_source_group',
			'required' => 1,
		),
		'send_email' => array(
			'type'     => 'static',
			'label'    => 'Send customer email when setting new group',
			'input'    => 'select',
			'source'   => 'eav/entity_attribute_source_boolean',
			'required' => 0,
		),
		'email_template' => array(
			'type'     => 'static',
			'label'    => 'Email Template',
			'input'    => 'select',
			'source'   => 'GroupSwitcher/entity_attribute_source_email_template',
			'required' => 0,
		),
		'comment' => array(
			'type'     => 'static',
			'label'    => 'Comment',
			'input'    => 'textarea',
			'required' => 0,
		),
		'created_at' => array(
			'type'     => 'static',
			'label'    => 'Created At',
			'input'    => 'label',
			'required' => 0,
		),
		'updated_at' => array(
			'type'     => 'static',
			'label'    => 'Updated At',
			'input'    => 'label',
			'required' => 0,
		),
)) as $entityType => $attributes) {
	foreach ($attributes as $attributeCode => $attributeData) {
		$this->addAttribute($entityType, $attributeCode, $attributeData);
	}
}

$this->endSetup();