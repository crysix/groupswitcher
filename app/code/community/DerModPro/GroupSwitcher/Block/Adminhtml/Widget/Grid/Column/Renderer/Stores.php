<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Widget_Grid_Column_Renderer_Stores
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
		if ($value = parent::_getValue($row))
		{
			$storeIds = explode(',', $value);
			$codes = array();
			foreach ($storeIds as $storeId)
			{
				if ($store = Mage::app()->getStore($storeId))
				{
					$codes[] = $this->htmlEscape($store->getName());
				}
			}
		
			$value = implode("<br/>\n", $codes);
		}
		return $value;
    }
}

