<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Type extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Abstract
{
	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('type');
		
		$fieldset = $form->addFieldset('type_form', array(
			'legend' => Mage::helper('GroupSwitcher')->__('Rule Type Setup')
		));

		$attributes = $this->_getGroupAttributes('Rule Type');


		$fieldset->addField('explanation', 'label', array(
				'label'     => '',
				'name'      => 'explanation',
				'after_element_html' => $this->_getExplanationHtml(),
		));

        $this->_setFieldset($attributes, $fieldset);
        $form->addValues($this->_getFormData());
		$this->setForm($form);

		return parent::_prepareForm();
	}

	protected function _getExplanationHtml()
	{
		$text = '';

		if ($class = Mage::registry('groupswitcher_rule')->getTypeBlockClass())
		{
			$text .= Mage::helper('GroupSwitcher')->__('Rule block class not found: <strong>%s</strong>', $class);
		}
		else
		{
			$text .= Mage::helper('GroupSwitcher')->__('No rule block class configured for this rule type');
		}

		$text .= '<br/><br/>';
		$text .= Mage::helper('GroupSwitcher')->__('This is a sample form template. Each rule should implement there own form with the fields that make sense for that rule.');

		return $text;
	}
}