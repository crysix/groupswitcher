<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_CustomerMatch
	extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_Abstract
{

	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('type');
		$this->setForm($form);

		$fieldset = $form->addFieldset('type_form', array(
			'legend' => $this->_getRuleModel()->getTypeModel()->getLabel()
		));

		$fieldset->addField('rule_value1', 'select', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Customer Attribute'),
			'name'     => 'rule_value1',
			'options'  => $this->_getCustomerAttributesAsOptions(),
			'required' => 1,
		));

		$fieldset->addField('rule_value2', 'text', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Matches'),
			'name'     => 'rule_value2',
			'required' => 1,
		));

		$fieldset->addField('rule_value3', 'select', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Match Expression Type'),
			'name'     => 'rule_value3',
			'options'  => $this->_getMatchTypeOptions(),
			'note'     =>
				Mage::helper('GroupSwitcher')->__('If regular expression is selected, enter a valid <a href="http://www.php.net/manual/en/function.preg-match.php" target="_blank">PCRE</a>.') . '<br/>' .
				Mage::helper('GroupSwitcher')->__('If callback is selected, the expression format is:<br/><strong>module/model::method</strong>') . '<br/>' .
				Mage::helper('GroupSwitcher')->__('The arguments passed to the method are the customer model and the selected attribute code.'),
			'required' => 0,
		));

        $form->addValues($this->_getFormData());

		return parent::_prepareForm();
	}

	protected function _getCustomerAttributesAsOptions()
	{
		$customerEntityType = Mage::getModel('eav/entity_type')->loadByCode('customer');
		$options = array(
			'' => Mage::helper('GroupSwitcher')->__('-- Please Choose --')
		);
		foreach ($customerEntityType->getAttributeCollection() as $attribute)
		{
			if ($attribute->getFrontendLabel())
			{
				$options[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
			}
		}
		asort($options);
		return $options;
	}

	protected function _getMatchTypeOptions()
	{
		return $this->_getRuleModel()->getTypeModel()->getMatchTypeOptions();
	}
}
