<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Schedule extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Abstract
{
	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('schedule');

		
		$fieldset = $form->addFieldset('schedule_form', array(
			'legend' => Mage::helper('GroupSwitcher')->__('Schedule Setup')
		));

		$attributes = $this->_getGroupAttributes('Schedule Switch');
        $this->_setFieldset($attributes, $fieldset);
        $form->addValues($this->_getFormData());
		$this->setForm($form);

		/*
		 * Define field dependencies
		 */
		$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
			->addFieldMap("scheduleschedule_switch", 'schedule_switch')
			->addFieldMap("scheduleschedule_switch_days", 'schedule_switch_days')
			->addFieldMap("scheduleschedule_group_id_before", 'schedule_group_id_before')
			->addFieldMap("scheduleschedule_group_id_after", 'schedule_group_id_after')
			->addFieldMap("scheduleschedule_replace_customer_switch", 'schedule_replace_customer_sw')
			->addFieldMap("scheduleschedule_send_email", 'schedule_send_email')
			->addFieldMap("scheduleschedule_email_template", 'schedule_email_template')
			->addFieldDependence('schedule_switch_days', 'schedule_switch', '1')
			->addFieldDependence('schedule_group_id_before', 'schedule_switch', '1')
			->addFieldDependence('schedule_group_id_after', 'schedule_switch', '1')
			->addFieldDependence('schedule_replace_customer_sw', 'schedule_switch', '1')
			->addFieldDependence('schedule_send_email', 'schedule_switch', '1')
			->addFieldDependence('schedule_email_template', 'schedule_switch', '1')
			->addFieldDependence('schedule_email_template', 'schedule_send_email', '1')
		);

		return parent::_prepareForm();
	}
}