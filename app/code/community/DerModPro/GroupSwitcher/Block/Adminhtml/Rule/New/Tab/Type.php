<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_New_Tab_Type extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('rule_form', array(
			'legend' => Mage::helper('GroupSwitcher')->__('Rule Type')
		));

		$fieldset->addField('rule_type', 'select', array(
				'label'     => Mage::helper('GroupSwitcher')->__('Rule Type'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'rule_type',
				'options'	=> $this->_getRuleTypeOptionsHash(false),
		));

		$fieldset->addField('continue', 'label', array(
				'label'     => '',
				'name'      => 'continue',
				'after_element_html' => $this->_getSubmitButtonHtml(),
		));
 
		return parent::_prepareForm();
	}

	protected function _getRuleTypeOptionsHash()
	{
		$options = array('' => Mage::helper('GroupSwitcher')->__('-- Please Choose --'));
		$types = Mage::helper('GroupSwitcher')->getAllRuleTypes();
		foreach ($types as $value => $type)
		{
			if (isset($type['hidden']) && $type['hidden'])
			{
				continue;
			}
			$options[$value] = $type['label'];
		}
		return $options;
	}

	protected function _getSubmitButtonHtml()
	{
		return '<button type="button" class="scalable save" onclick="editForm.submit()"><span>' .
			Mage::helper('GroupSwitcher')->__('Continue') .
			'</span></button>';
	}
}