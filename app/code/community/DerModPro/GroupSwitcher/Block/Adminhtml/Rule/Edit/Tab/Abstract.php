<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

abstract class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Abstract extends Mage_Adminhtml_Block_Widget_Form
{
	/**
	 *
	 * @var array
	 */
	protected $_attributes;

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Rule
	 */
	protected function _getRuleModel()
	{
		return Mage::registry('groupswitcher_rule');
	}

	/**
	 *
	 * @return array
	 */
	protected function _getFormData()
	{
		$data = Mage::getSingleton('adminhtml/session')->getFormData();

		if (! $data && $this->_getRuleModel()->getData())
		{
			$data = $this->_getRuleModel()->getData();
		}

		return (array) $data;
	}

	/**
	 *
	 * @param string $group
	 * @return array
	 */
	protected function _getGroupAttributes($group)
	{
		$attributes = array();

		foreach ($this->_getAttributes() as $attribute)
		{
			if ($attribute->getGroup() == $group || (! $attribute->getGroup() && $group == 'General'))
			{
				if ($attribute->getFrontendInput() == 'label' && ! $this->_getRuleModel()->getId())
				{
					continue;
				}
				$attributes[] = $attribute;
			}
		}

		return $attributes;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getAttributes()
	{
		if (is_null($this->_attributes))
		{
			$this->_attributes = $this->_getRuleModel()->getAttributes();
		}
		return $this->_attributes;
	}

	/**
	 *
	 * @param string $code
	 * @return Mage_Eav_Model_Entity_Attribute
	 */
	protected function _getAttributeByCode($code)
	{
		$attributes = $this->_getAttributes();
		if (isset($attributes[$code]))
		{
			return $attributes[$code];
		}

		return false;
	}

	/**
	 * Translate attribute notes before passing the method call to the parent.
	 *
     * @param array $attributes attributes that are to be added
     * @param Varien_Data_Form_Element_Fieldset $fieldset
     * @param array $exclude attributes that should be skipped
	 * @return null
	 */
	protected function _setFieldset($attributes, $fieldset, $exclude = array())
	{
		foreach ($attributes as $attribute)
		{
			if ($note = $attribute->getNote())
			{
				$attribute->setNote(Mage::helper('GroupSwitcher')->__($note));
			}
		}
		return parent::_setFieldset($attributes, $fieldset, $exclude);
	}
}

