<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

abstract class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_Abstract
	extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Abstract
{
	protected function _getStoreSelectorOptions()
	{
		$optGroups = array();

		foreach (Mage::app()->getWebsites() as $website)
		{
			/* @var $website Mage_Core_Model_Website */
			foreach ($website->getGroups() as $group)
			{
				$options = array();
				
				/* @var $group Mage_Core_Model_Store_Group */
				foreach ($group->getStores() as $store)
				{
					/* @var $store Mage_Core_Model_Store */
					$options[] = array(
						'label' => $store->getName(),
						'title' => $store->getName(),
						'value' => $store->getId(),
						'style' => 'padding-left: 30px;',
					);
				}

				if (count($options))
				{
					$optGroups[] = array(
						'label' => $website->getName(),
						'value' => array()
					);
					$optGroups[] = array(
						'label' => str_repeat('&nbsp;', 4) . $group->getName(),
						'value' => $options
					);
				}
			}
		}
		return $optGroups;
	}
}

