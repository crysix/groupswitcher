<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Schedule_Switch_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('scheduleGrid');
		$this->setUseAjax(false);
		$this->setDefaultSort('entity_id');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel('GroupSwitcher/schedule_switch_collection')
            ->addCustomerData();
		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('entity_id', array(
			'header' => Mage::helper('GroupSwitcher')->__('ID'),
			'width' => '50px',
			'index' => 'entity_id',
			'type' => 'number',
			'filter_index' => 'main_table.entity_id',
		));

		$this->addColumn('name', array(
			'header' => Mage::helper('GroupSwitcher')->__('Customer'),
			'index' => 'name',
			'filter_index' => 'email',
			'filter' => false,
		));

		$this->addColumn('email', array(
			'header' => Mage::helper('GroupSwitcher')->__('Email'),
			'index' => 'email',
		));
		
		if (Mage::getSingleton('customer/config_share')->isWebsiteScope())
		{
			$this->addColumn('customer_website_id', array(
				'header' => Mage::helper('GroupSwitcher')->__('Website'),
				'index' => 'customer_website_id',
				'type' => 'options',
				'options' => $this->_getWebsiteOptions(),
			));
		}

		$this->addColumn('date', array(
			'header' => Mage::helper('GroupSwitcher')->__('Switch Date'),
			'type' => 'date',
			'align' => 'center',
			'index' => 'date',
			'gmtoffset' => true,
		));

		$this->addColumn('group_before', array(
			'header' => Mage::helper('GroupSwitcher')->__('From Group'),
			'index' => 'group_id_before',
			'type' => 'options',
			'options' => $this->_getGroupBeforeOptions(),
		));

		$this->addColumn('group_after', array(
			'header' => Mage::helper('GroupSwitcher')->__('To Group'),
			'index' => 'group_id_after',
			'type' => 'options',
			'options' => $this->_getGroupAfterOptions(),
		));

		$this->addColumn('send_email', array(
			'header' => Mage::helper('GroupSwitcher')->__('Send Email'),
			'width' => '50px',
			'index' => 'send_email',
			'type' => 'options',
			'options' => $this->_getYesNoOptions(),
		));

		$this->addColumn('rule_type', array(
			'header' => Mage::helper('GroupSwitcher')->__('Note'),
			'index' => 'comment',
			'type' => 'longtext',
			'nl2br' => 0,
			'escape' => 1,
			'sortable' => false,
		));
		

		$this->addColumn('action', array(
			'header' => Mage::helper('GroupSwitcher')->__('Action'),
			'width' => '100',
			'type' => 'action',
			'getter' => 'getId',
			'actions' => array(
				array(
					'caption' => Mage::helper('GroupSwitcher')->__('Edit'),
					'url' => array('base' => '*/*/edit'),
					'field' => 'id'
				)
			),
			'filter' => false,
			'sortable' => false,
			'index' => 'stores',
			'is_system' => true,
		));
		
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

	protected function _getYesNoOptions()
	{
		return array(
			1 => Mage::helper('GroupSwitcher')->__('Yes'),
			0 => Mage::helper('GroupSwitcher')->__('No'),
		);
	}

	protected function _getGroupOptions()
	{
		$groups = $this->getCustomerGroupHash();
		if (is_null($groups))
		{
			$groups = Mage::getResourceModel('customer/group_collection')
				->addFieldToFilter('customer_group_id', array('gt'=> 0))
				->load()
				->toOptionHash();
			$this->setCustomerGroupHash($groups);
		}
		return $groups;
	}

	protected function _getGroupBeforeOptions()
	{
		$groups = $this->_getGroupOptions();
		ksort($groups);
		return $groups;
	}

	protected function _getGroupAfterOptions()
	{
		$groups = $this->_getGroupOptions();
		//$groups['0'] = Mage::helper('GroupSwitcher')->__('-- No Change --');
		ksort($groups);
		return $groups;
	}

	protected function _getWebsiteOptions()
	{
		$websites = $this->getWebsiteOptionHash();
		if (is_null($websites))
		{
			$websites = Mage::getResourceModel('core/website_collection')->load()->toOptionHash();
			$this->setWebsiteOptionHash($websites);
		}
		return $websites;
	}
}

