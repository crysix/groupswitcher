<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Schedule_Switch_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('switch_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('GroupSwitcher')->__('Scheduled Switch Setup'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('general_section', array(
				'label'   => Mage::helper('GroupSwitcher')->__('General'),
				'title'   => Mage::helper('GroupSwitcher')->__('General'),
				'content' => $this->getLayout()->createBlock('GroupSwitcher/adminhtml_schedule_switch_edit_tab_general')->toHtml(),
		));

		return parent::_beforeToHtml();
	}
}