<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Schedule_Switch_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'GroupSwitcher';
		$this->_controller = 'adminhtml_schedule_switch';
		$this->_mode = 'edit';
		
		$this->_updateButton('save', 'label', Mage::helper('GroupSwitcher')->__('Save Scheduled Switch'));
		$this->_updateButton('delete', 'label', Mage::helper('GroupSwitcher')->__('Delete Scheduled Switch'));

		$this->_addButton('save_and_continue', array(
				'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
				'onclick' => 'saveAndContinueEdit()',
				'class' => 'save',
		), -100);

        $this->_formScripts[] = "

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

	public function getHeaderText()
	{
		if (Mage::registry('groupswitcher_switch') && Mage::registry('groupswitcher_switch')->getId())
		{
			return Mage::helper('GroupSwitcher')->__('Edit Scheduled Switch #%d', $this->htmlEscape(Mage::registry('groupswitcher_switch')->getId()));
		}
		else
		{
			return Mage::helper('GroupSwitcher')->__('New Scheduled Switch');
		}
	}
}